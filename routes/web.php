<?php

use App\Http\Controllers\Panel\CertificatesController;
use App\Http\Controllers\Panel\UsersController;
use App\Http\Controllers\Panel\AssesorsController as PanelAsseorsController;
use App\Http\Controllers\Panel\EventActifitiesController;
use App\Http\Controllers\Panel\PagesController as PanelPagesController;
use App\Http\Controllers\Panel\PostsController as PanelPostsController;
use App\Http\Controllers\Panel\SchemesController as PanelSchemesController;
use App\Http\Controllers\Public\AssesorsController;
use App\Http\Controllers\Public\CertifactesController;
use App\Http\Controllers\Public\EventsController;
use App\Http\Controllers\Public\HomeController;
use App\Http\Controllers\Public\PagesController;
use App\Http\Controllers\Public\PostsController;
use App\Http\Controllers\Public\SchemesController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('homepage');
Route::get('/certificates', [CertifactesController::class, 'index'])->name('public.certificates');
Route::get('/assesors', [AssesorsController::class, 'index'])->name('public.assesors');
Route::get('/articles', [PostsController::class, 'index'])->name('public.articles');
Route::get('/articles/{post}', [PostsController::class, 'read'])->name('public.articles.read');
Route::get('/pages/{page}', [PagesController::class, 'index'])->name('public.pages.read');
Route::get('/events', [EventsController::class, 'index'])->name('public.events');
Route::get('/events/{slug}', [EventsController::class, 'view'])->name('public.events.view');
Route::get('/schemes/{slug?}', [SchemesController::class, 'index'])->name('public.schemes');


Route::prefix('/panel')->group(function () {
    // Dashboard
    Route::get('/dashboard', function () {
        return view('pages/panel/dashboard');
    })->middleware(['auth'])->name('dashboard');

    // route for manage users
    Route::get('/users', [UsersController::class, 'index'])->middleware(['auth'])->name('users');
    Route::get('/users/create', [UsersController::class, 'create'])->middleware(['auth'])->name('users.add');
    Route::post('/users/create', [UsersController::class, 'store'])->middleware(['auth'])->name('users.store');
    Route::get('/users/{user}', [UsersController::class, 'edit'])->middleware(['auth'])->name('users.edit');
    Route::put('/users/{user}', [UsersController::class, 'update'])->middleware(['auth'])->name('users.update');
    Route::delete('/users/{user}', [UsersController::class, 'delete'])->middleware(['auth'])->name('users.delete');

    // route for manage certificates
    Route::get('/certificates', [CertificatesController::class, 'index'])->middleware(['auth'])->name('certificates');
    Route::get('/certificates/create', [CertificatesController::class, 'create'])->middleware(['auth'])->name('certificates.add');
    Route::post('/certificates/create', [CertificatesController::class, 'store'])->middleware(['auth'])->name('certificates.store');
    Route::get('/certificates/{certificate}', [CertificatesController::class, 'edit'])->middleware(['auth'])->name('certificates.edit');
    Route::put('/certificates/{certificate}', [CertificatesController::class, 'update'])->middleware(['auth'])->name('certificates.update');
    Route::delete('/certificates/{certificate}', [CertificatesController::class, 'destroy'])->middleware(['auth'])->name('certificates.destroy');

    // route for manage assesors
    Route::get('/assesors', [PanelAsseorsController::class, 'index'])->middleware(['auth'])->name('assesors');
    Route::get('/assesors/create', [PanelAsseorsController::class, 'create'])->middleware(['auth'])->name('assesors.add');
    Route::post('/assesors/create', [PanelAsseorsController::class, 'store'])->middleware(['auth'])->name('assesors.store');
    Route::get('/assesors/{assesor}', [PanelAsseorsController::class, 'edit'])->middleware(['auth'])->name('assesors.edit');
    Route::put('/assesors/{assesor}', [PanelAsseorsController::class, 'update'])->middleware(['auth'])->name('assesors.update');
    Route::delete('/assesors/{assesor}', [PanelAsseorsController::class, 'destroy'])->middleware(['auth'])->name('assesors.destroy');

    // route for manage assesors
    Route::get('/schemes', [PanelSchemesController::class, 'index'])->middleware(['auth'])->name('schemes');
    Route::get('/schemes/create', [PanelSchemesController::class, 'create'])->middleware(['auth'])->name('schemes.add');
    Route::post('/schemes/create', [PanelSchemesController::class, 'store'])->middleware(['auth'])->name('schemes.store');
    Route::get('/schemes/{scheme}', [PanelSchemesController::class, 'edit'])->middleware(['auth'])->name('schemes.edit');
    Route::put('/schemes/{scheme}', [PanelSchemesController::class, 'update'])->middleware(['auth'])->name('schemes.update');
    Route::delete('/schemes/{scheme}', [PanelSchemesController::class, 'destroy'])->middleware(['auth'])->name('schemes.destroy');

    // route for manage posts
    Route::get('/posts', [PanelPostsController::class, 'index'])->middleware(['auth'])->name('posts');
    Route::get('/posts/create', [PanelPostsController::class, 'create'])->middleware(['auth'])->name('posts.add');
    Route::post('/posts/create', [PanelPostsController::class, 'store'])->middleware(['auth'])->name('posts.store');
    Route::get('/posts/{post}', [PanelPostsController::class, 'edit'])->middleware(['auth'])->name('posts.edit');
    Route::put('/posts/{post}', [PanelPostsController::class, 'update'])->middleware(['auth'])->name('posts.update');
    Route::delete('/posts/{post}', [PanelPostsController::class, 'destroy'])->middleware(['auth'])->name('posts.destroy');
    
    // route for manage posts
    Route::get('/events', [EventActifitiesController::class, 'index'])->middleware(['auth'])->name('events');
    Route::get('/events/create', [EventActifitiesController::class, 'create'])->middleware(['auth'])->name('events.add');
    Route::post('/events/create', [EventActifitiesController::class, 'store'])->middleware(['auth'])->name('events.store');
    Route::get('/events/{event}', [EventActifitiesController::class, 'edit'])->middleware(['auth'])->name('events.edit');
    Route::put('/events/{event}', [EventActifitiesController::class, 'update'])->middleware(['auth'])->name('events.update');
    Route::delete('/events/{event}', [EventActifitiesController::class, 'destroy'])->middleware(['auth'])->name('events.destroy');

    // route for manage posts
    Route::get('/pages', [PanelPagesController::class, 'index'])->middleware(['auth'])->name('pages');
    Route::get('/pages/create', [PanelPagesController::class, 'create'])->middleware(['auth'])->name('pages.add');
    Route::post('/pages/create', [PanelPagesController::class, 'store'])->middleware(['auth'])->name('pages.store');
    Route::get('/pages/{page}', [PanelPagesController::class, 'edit'])->middleware(['auth'])->name('pages.edit');
    Route::put('/pages/{page}', [PanelPagesController::class, 'update'])->middleware(['auth'])->name('pages.update');
    
});

Route::get("/run-migration", function() {
    Artisan::call("migrate:refresh", ['--seed' => true]);
    echo "migration success";
});

Route::get("/clear-cache", function() {
    Artisan::call("cache:clear");
    echo "clear cache success";
});

Route::get("/route-cache", function() {
    Artisan::call("route:clear");
    echo "clear route cache success";
});

Route::get("/storage-link", function() {
    Artisan::call("storage:link");
    echo "storage link created";
});

require __DIR__ . '/auth.php';
