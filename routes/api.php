<?php

use App\Http\Controllers\Api\UploadImageController;
use App\Http\Controllers\Public\ContactsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('contact', [ContactsController::class, 'create'])->name("create.contact");

Route::post('images/upload', [UploadImageController::class, 'upload']);
Route::post('images/delete', [UploadImageController::class, 'delete']);