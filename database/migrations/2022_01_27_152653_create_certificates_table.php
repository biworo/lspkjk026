<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->id();
            $table->string('no_reg', 50)->unique();
            $table->string('name', 50);
            $table->enum('gender', ['m', 'f']);
            $table->string('no_cert', 50);
            $table->string('no_blanko', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('phone', 15)->nullable();
            $table->integer('scheme_id')->nullable();
            $table->integer('sector_id');
            $table->integer('province_id');
            $table->string('year');
            $table->string('expire_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
