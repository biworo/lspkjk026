<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssesorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assesors', function (Blueprint $table) {
            $table->id();
            $table->char("no_registration", 75);
            $table->string("name", 225);
            $table->string("no_certificate", 225);
            $table->string("no_blanko", 225);
            $table->string("email", 225);
            $table->string("handphone", 225);
            $table->integer("sector_id");
            $table->integer("province_id");
            $table->year("year");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assesors');
    }
}
