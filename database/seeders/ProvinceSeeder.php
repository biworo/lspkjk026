<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            ["id" => 11, "name" => "Aceh", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 12, "name" => "Sumatera Utara", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 13, "name" => "Sumatera Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 14, "name" => "Riau", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 15, "name" => "Jambi", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 16, "name" => "Sumatera Selatan", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 17, "name" => "Bengkulu", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 18, "name" => "Lampung", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 19, "name" => "Kepulauan Bangka Belitung", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 21, "name" => "Kepulauan Riau", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 31, "name" =>    "DKI Jakarta", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 32, "name" => "Jawa Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 33, "name" => "Jawa Tengah", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 34, "name" =>    "DI Yogyakarta", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 35, "name" => "Jawa Timur", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 36, "name" => "Banten", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 51, "name" => "Bali", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 52, "name" => "Nusa Tenggara Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 53, "name" => "Nusa Tenggara Timur", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 61, "name" => "Kalimantan Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 62, "name" => "Kalimantan Tengah", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 63, "name" => "Kalimantan Selatan", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 64, "name" => "Kalimantan Timur", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 65, "name" => "Kalimantan Utara", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 71, "name" => "Sulawesi Utara", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 72, "name" => "Sulawesi Tengah", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 73, "name" => "Sulawesi Selatan", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 74, "name" => "Sulawesi Tenggara", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 75, "name" => "Gorontalo", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 76, "name" => "Sulawesi Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 81, "name" => "Maluku", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 82, "name" => "Maluku Utara", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 91, "name" => "Papua Barat", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
            ["id" => 94, "name" => "Papua", "created_at" => Carbon::now(), "updated_at" => Carbon::now()],
        ];

        foreach ($provinces as $province) {
            DB::table('provinces')->insert($province);
        }
    }
}
