<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssesorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $query = "INSERT INTO `assesors` (`id`, `no_registration`, `name`, `no_certificate`, `no_blanko`, `email`, `handphone`, `sector_id`, `province_id`, `year`)
        VALUES
            (1, 'MET.000.000632.2009', 'Suryanto', '93000.2419.0021355.2014', 651368, 'babut4547@yahoo.co.id', 2147483647, 1, 33, '2009'),
            (3, 'MET.000.002105.2008', 'Surachmad', '93000.2419.0021353.2014', 651366, 'surachmaddm@yahoo.co.id', 2147483647, 1, 35, '2008'),
            (5, 'MET.000.001178.2009', 'Agustian Tjahjanto', '93000.2419.0021361.2014', 651374, 'agustian.thahjanto8@gmail.com', 818519787, 1, 35, '2009'),
            (8, 'MET.000.000959.2008', 'Eko Kawandono', '93000.2419.0021350.2014', 651363, 'eko_kawandono@yaoo.co.id', 816728823, 1, 31, '2008'),
            (10, 'MET.000.002103.2008', 'Yusuf Sofyan', '93000.2419.0021352.2014', 651365, 'yus_sofyan@yahoo.co.id', 2147483647, 1, 35, '2008'),
            (11, 'MET.000.002455.2011', 'Bambang Sugeng', '93000.2419.0021365.2014', 651378, 'wanamuktig216@yahoo.co.id', 2147483647, 1, 33, '2011'),
            (12, 'MET.000.002478.2011', 'Komariah', '93000.2419.0021367.2014', 651380, 'dra.komariyah@yahoo.com', 816676713, 1, 33, '2011'),
            (13, 'MET.000.000953.2008', 'Wardjono', '93000.2419.0021348.2014', 651361, 'wardjono58@gmail.com', 2147483647, 1, 31, '2008'),
            (14, 'MET.000.002481.2011', 'Endro Praponco', '93000.2419.0021366.2014', 651379, 'endropraponco@gmail.com', 2147483647, 1, 31, '2011'),
            (15, 'MET.000.001154.2009', 'Muliarsa Mandala', '93000.2419.0021357.2014', 651370, 'mandala.muliarsa@yahoo.com', 2147483647, 1, 51, '2009'),
            (17, 'MET.000.001171.2009', 'Sunaryo', '93000.24190021360.2014', 651373, 'naryolampung56@gmail.com', 2147483647, 1, 18, '2009'),
            (18, 'MET.000.001182.2009', 'Kamarudin', '93000.2419.0021363.2014', 651376, 'kamaruddin0807@gmail.com', 2147483647, 1, 73, '2009'),
            (21, 'MET.000.000771.2006', 'LSS Wahyu Anindya', '93000.2419.0021580.2014', 690230, 'wahyu_anindya@yahoo.com', 2147483647, 1, 31, '2006'),
            (22, 'MET.000.001516.2016', 'Agung Sutresni Djelantik', '93000.2419.0038305.2016', 2884446, 'agungdjelantik@gmail.com', 2147483647, 1, 51, '2016'),
            (23, 'MET.000.001523.2016', 'Puji Astuti', '93000.2419.0038312.2016', 2884453, 'puti.mns@gmail.com', 2147483647, 1, 33, '2016'),
            (25, 'MET.000.003098.2016', 'M. Nedy Afriady', '93000.2419.0040431.2016', 2899883, 'nedy.afriady74@gmail.com', 2147483647, 1, 12, '2016'),
            (26, 'MET.000.003097.2016', 'Syamsiar H. Sandiah', '93000.2419.0040430.2016', 2899882, 'sandiah.syamriar@gmail.com', 2147483647, 1, 32, '2016'),
            (28, 'MET.000.011788 2016', 'Amin Suhandi', '93000 2419 0050291 2016', 3195245, 'aminsuhadi.026@gmail.com', 2147483647, 1, 18, '2016'),
            (29, 'MET.000.011789 2016', 'Kiki Lestari', '93000 2419 0050292 2016', 3195246, 'kikilestari.026@gmail.com', 2147483647, 1, 12, '2016'),
            (32, 'MET.000.011792 2016', 'Darmeli Nasution', ' 93000 2419 0050295 2016', 0, 'darmelinasution.026@gmail.com', 2147483647, 1, 12, '2016'),
            (33, 'MET.000.011793 2016', 'Budiyono', '93000 2419 0050296 2016', 3195250, 'budiyono.026@gmail.com', 2147483647, 1, 33, '2016'),
            (34, 'MET.000.011794 2016', 'Antonius Sudirman Zai', ' 93000 2419 0050297 2016', 3195251, 'antoniussudirmanzai.026@gmail.com', 2147483647, 1, 12, '2016'),
            (36, 'MET.000.011796 2016', 'Abdul Haris', '93000 2419 0050299 2016', 3195253, 'abdulharis.026@gmail.com', 2147483647, 1, 34, '2016'),
            (37, 'MET.000.011797 2016', 'Ujang', '93000 2419 0050300 2016', 3195254, '', 2147483647, 1, 31, '2016'),
            (39, 'MET.000.011799 2016', 'I Wayan Suyatna', '93000 2419 0050302 2016', 0, '', 2147483647, 1, 51, '2016'),
            (40, 'MET.000.011800 2016', 'Abdul Muslih', ' 93000 2419 0050303 2016', 3195257, '', 2147483647, 1, 33, '2016'),
            (41, 'MET.000.011801 2016', 'Muhammad Ramlan', '93000 2419 0050304 2016', 3195258, '', 2147483647, 1, 63, '2016'),
            (42, 'MET.000.011802 2016', 'Ratnawati', ' 93000 2419 0050305 2016', 3195259, '', 2147483647, 1, 36, '2016'),
            (45, 'MET.000.011805 2016', 'Ahsanal Huda', ' 93000 2419 0050308 2016', 3195262, '', 2147483647, 1, 18, '2016'),
            (47, 'MET.000.011807 2016', 'Mardijati', '93000 2419 0050310 2016', 3195264, '  ', 2147483647, 1, 33, '2016'),
            (48, 'MET.000.011808 2016', 'Yudi Heriyanto', ' 93000 2419 0050311 2016', 3195265, '', 2147483647, 1, 32, '2016'),
            (50, 'MET.000.011810 2016', 'Suharno', ' 93000 2419 0050313 2016', 3195267, '', 2147483647, 1, 32, '2016'),
            (51, 'MET.000.011811 2016', 'Efendi Damanik', '93000 2419 0050314 2016', 3195268, '', 2147483647, 1, 12, '2016'),
            (52, 'MET.000.011812 2016', 'Y. Gede Sutmasa', ' 93000 2419 0050315 2016', 0, '', 2147483647, 1, 32, '2016'),
            (53, 'MET.000.011813 2016', 'Dewa Gede Widnyana Putra', '93000 2419 0050316 2016', 0, '', 2147483647, 1, 51, '2016'),
            (55, 'MET.000.011815 2016', 'Puryadi', ' 93000 2419 0050318 2016', 0, '', 2147483647, 1, 36, '2016'),
            (56, 'MET.000.011816 2016', 'E. Farida', '93000 2419 0050319 2016', 3195273, '', 2147483647, 1, 36, '2016'),
            (57, 'MET.000.011817 2016', 'Bambang Wahyudiono', ' 93000 2419 0050320 2016', 3195274, '', 2147483647, 1, 36, '2016'),
            (59, 'MET.000.011819 2016', 'Elly Kasim', ' 93000 2419 0050322 2016', 3195276, '', 2147483647, 1, 18, '2016'),
            (60, 'MET.000.002141.2019', 'Hena Anditya ', '4340051', 4340051, 'henaanditya.026@gmail.com', 2147483647, 1, 51, '2019'),
            (61, 'MET.000.002143.2019', 'Ria Indhirawati', '4336112', 4336112, 'riaindhirawati.026@gmail.com', 2147483647, 1, 35, '2019'),
            (62, 'MET.000.002144.2019', 'Farade Kiat Sudrajat ', '4491505', 4491505, 'faradekiatsudrajat.026@gmail.com', 2147483647, 1, 35, '2019'),
            (63, 'MET.000.002145.2019', 'Dian Septiana Sari ', '4348844', 434844, 'dianseptianasari.026@gmail.com', 2147483647, 1, 12, '2019'),
            (64, 'MET.000.002147.2019', 'Windardi', '4348845', 4348845, 'windardi.026@gmail.com', 2147483647, 1, 32, '2019'),
            (65, 'MET.000.002149.2019', 'Dwi Sumiarso', '4340050', 4340050, 'dwisumiarso.026@gmail.com', 2147483647, 1, 35, '2019'),
            (66, 'MET.000.002150.2019', 'Anom Prasetyo', '4336109', 4336109, 'anomprasetyo.026@gmail.com', 2147483647, 1, 51, '2019'),
            (67, 'MET.000.002151.2019', 'Bambang Sigit Pramono ', '4336111', 4336111, 'bambangsigitpramono.026@gmail.com', 2147483647, 1, 35, '2019'),
            (68, 'MET.000.002152.2019', 'Nurafrina Siregar ', '4348843', 4348843, 'nurafrinasiregar.026@gmail.com', 2147483647, 1, 12, '2019'),
            (69, 'MET.000.002153.2019', 'Ollan Hardlan ', '4340053', 4340053, 'ollanhardlan.026@gmail.com', 2147483647, 1, 35, '2019'),
            (70, 'MET.000.002154.2019', 'Sulistia Suwondo Hartiawan ', '3584586', 3584586, 'sulistiasuwondohartiawan.026@gmail.com', 2147483647, 1, 32, '2019'),
            (71, 'MET.000.002155.2019', 'Jordy Pamungkas Aknar ', '4348846', 4348846, 'jordypamungkasakbar.026@gmail.com', 2147483647, 1, 31, '2019'),
            (72, 'MET.000.002155.2019', 'Jordy Pamungkas Akbar ', '4348846', 4348846, 'jordypamungkasakbar.026@gmail.com', 2147483647, 1, 31, '2019'),
            (73, 'MET.000.002156.2019', 'Muzayad Lutfi Noor ', '3132540', 3132540, 'muzayadlutfinoor.026@gmail.com', 2147483647, 1, 33, '2019'),
            (74, 'MET.000.002157.2019', 'Moch. Djamil ', '4348847', 4348847, 'mochdjamil.026@gmail.com', 2147483647, 1, 35, '2019'),
            (75, 'MET.000.002158.2019', 'Achmad Rizadi', '4340052', 4340052, 'achmadrizadi.026@gmail.com', 2147483647, 1, 35, '2019'),
            (76, 'MET.000.002159.2019', 'Nur Cholis ', '4340054', 4340054, 'nurcholis.026@gmail.com', 2147483647, 1, 35, '2019'),
            (77, 'MET.000.002160.2019', 'Abd. Goni ', '4336113', 4336113, 'abdgoni.026@gmail.com', 2147483647, 1, 35, '2019');";

        DB::unprepared($query);
    }
}
