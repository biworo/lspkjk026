<?php

namespace Database\Seeders;

use App\Models\scheme;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schemes = [
            [
                'code' => 'SKM-026-001',
                'name' => 'Kasir',
                'icon' => 'fas fa-cash-register',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
                'scheme_details' => [
                    [
                        'code' => 'KJK.SP01.001.01',
                        'title' => 'Melaksanakan Dasar-Dasar Manajemen',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP01.002.01',
                        'title' => 'Melakukan Prinsip-prinsip Pengelolaan Organisasi dan Manajemen Koperasi Jasa Keuangan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP01.004.01',
                        'title' => 'Melakukan Komunikasi',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.001.01',
                        'title' => 'Melakukan Transaksi Kas dan Non Kas',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.002.01',
                        'title' => 'Mengelola Tabungan/Simpanan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP03.001.01',
                        'title' => 'Mengoperasikan Komputer',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                ]
            ],
            [
                'code' => 'SKM-026-002',
                'name' => 'Juru Buku',
                'icon' => 'fas fa-book',
                'sector_id' => 1,
                'scheme_details' => [
                    [
                        'code' => 'KJK.SP01.001.01',
                        'title' => 'Melaksanakan Dasar-Dasar Manajemen',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP01.002.01',
                        'title' => 'Melakukan Prinsip-prinsip Pengelolaan Organisasi dan Manajemen Koperasi Jasa Keuangan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.001.01',
                        'title' => 'Melakukan Transaksi Kas dan Non Kas',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.004.01',
                        'title' => 'Mengerjakan Buku Besar dan Buku Pembantu',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP03.001.01',
                        'title' => 'Mengoperasikan Komputer',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                ]
            ],
            [
                'code' => 'SKM-026-003',
                'name' => 'Juru Tagih',
                'icon' => 'fas fa-people-arrows',
                'sector_id' => 1,
                'scheme_details' => [
                    [
                        'code' => 'KJK.SP01.001.01',
                        'title' => 'Melaksanakan Dasar-Dasar Manajemen',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP01.002.01',
                        'title' => 'Melakukan Prinsip-prinsip Pengelolaan Organisasi dan Manajemen Koperasi Jasa Keuangan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.006.01',
                        'title' => 'Melakukan Penagihan Angsuran',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.007.01',
                        'title' => 'Melakukan Pendampingan Usaha',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP03.003.01',
                        'title' => 'Menangani Pinjaman/Pembiayaan Bermasalah',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                ]
            ],
            [
                'code' => 'SKM-026-004',
                'name' => 'Juru Survey',
                'icon' => 'fas fa-person-booth',
                'sector_id' => 1,
                'scheme_details' => [
                    [
                        'code' => 'KJK.SP01.001.01',
                        'title' => 'Melaksanakan Dasar-Dasar Manajemen',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP01.002.01',
                        'title' => 'Melakukan Prinsip-prinsip Pengelolaan Organisasi dan Manajemen Koperasi Jasa Keuangan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.004.01',
                        'title' => 'Melakukan Komunikasi',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP02.007.01',
                        'title' => 'Melakukan Pendampingan Usaha',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP03.003.01',
                        'title' => 'Melakukan Survey Lapangan',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                    [
                        'code' => 'KJK.SP03.001.01',
                        'title' => 'Mengoperasikan Komputer',
                        // 'training_time_duration' => 2,
                        // 'test_time_duration' => 1,
                    ],
                ]
            ],
            [
                'code' => 'SKM-026-005',
                'name' => 'Customer Service',
                'icon' => 'fas fa-phone-volume',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' => 'SKM-026-006',
                'name' => 'Analis Pinjaman',
                'icon' => 'fas fa-landmark',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' => 'SKM-026-007',
                'name' => 'Petugas Pengendalian Intern',
                'icon' => 'fas fa-users-cog',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' => 'SKM-026-008',
                'name' => 'Kabag Dana',
                'icon' => 'fas fa-money-bill-wave',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' => 'SKM-026-009',
                'name' => 'Kabag Pinjaman/Pembiayaan',
                'icon' => 'fas fa-coins',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' =>  'SKM-026-010',
                'name' =>  'kabag Akuntansi',
                'icon' => 'fas fa-file-invoice',
                'sector_id' => 1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ],
            [
                'code' =>  'SKM-026-0011',
                'name' => 'Manajer/Kepala Cabang',
                'icon' => 'fas fa-hotel',
                'sector_id' =>  1,
                // 'created_at' => Carbon::now(),
                // 'updated_at' => Carbon::now(),
            ]
        ];
        $schemeModel = new scheme();
        foreach ($schemes as $scheme) {
            $details = isset($scheme['scheme_details']) ? $scheme['scheme_details'] : null;
            $sch = $schemeModel->create([
                'code' =>  $scheme['code'],
                'name' => $scheme['name'],
                'icon' => $scheme['icon'],
                'sector_id' =>  $scheme['sector_id']
            ]);
            if($details) {
                foreach($details as $detail) {
                    $detail['scheme_id'] = $sch->id;
                    $sch->schemeDetails()->create($detail);
                }
            }
        }
    }
}
