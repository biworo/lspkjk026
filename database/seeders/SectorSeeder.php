<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sectors = [
            [
                "name" => "Koperasi Jasa Keuangan",
                "desc" => "Koperasi Jasa Keuangan",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        ];
        foreach ($sectors as $sector) {
            DB::table('sectors')->insert($sector);
        }
    }
}
