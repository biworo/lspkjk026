<?php

namespace Database\Seeders;

use App\Models\EventActivity;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EventActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $events = [
            [
                'title' => 'Surveilance Lembaga Sertifikasi Profesi Koperasi Jasa Keuangan',
                'desc' => 'Terima kasih kepada tim Surveilance Badan Nasional Sertifikasi Profesi yang telah Surveilance di Lembaga Seritifikasi Profesi Koperasi Jasa Keuangan, pada tanggal 08 Mei 2018',
                'status' => 'publish',
                'slug' => Str::slug("Surveilance Lembaga Sertifikasi Profesi Koperasi Jasa Keuangan", "-"),
                'images' => [
                    '1/2.jpeg',
                    '1/3.jpeg',
                    '1/4.jpeg',
                    '1/5.jpeg',
                    '1/6.jpeg',
                ]
            ],
            [
                'title' => 'Uji Kompetensi Skema Kasir di Kebumen',
                'desc' => 'Telah dilakukan Uji Kompetensi Skema Kasir dengan jumlah peserta 30 (tiga puluh) orang di Hotel Candi Sari Kebumen, Jawa Tengah. Pada tanggal 13 - 14 September 2018',
                'status' => 'publish',
                'slug' => Str::slug("Uji Kompetensi Skema Kasir di Kebumen", "-"),
                'images' => [
                    '2/1.jpeg',
                    '2/2.jpeg',
                    '2/3.jpeg',
                    '2/4.jpeg',
                ]
            ],
            [
                'title' => 'Uji Kompetensi Skema Manajer di Semarang',
                'desc' => 'Telah dilakukan Uji Kompetensi Skema Manajer dengan jumlah peserta 26 (dua puluh enam) orang di Ungaran - Semarang. Pada tanggal 13 - 15 September 2018',
                'status' => 'publish',
                'slug' => Str::slug("Uji Kompetensi Skema Manajer di Semarang", "-"),
                'images' => [
                    '3/1.jpeg',
                    '3/2.jpeg',
                    '3/3.jpeg',
                    '3/4.jpeg',
                ]
            ],
            [
                "title" => 'KEGIATAN DIKLAT DAN UJI KOMPETENSI ASESOR DI PUNCAK VILLAGE - BOGOR ',
                "desc" => null,
                "status" => 'publish',
                'slug' => Str::slug("KEGIATAN DIKLAT DAN UJI KOMPETENSI ASESOR DI PUNCAK VILLAGE - BOGOR", "-"),
                'images' => [
                    '4/1.jpeg',
                    '4/2.jpeg',
                    '4/3.jpeg',
                    '4/4.jpeg',
                ]
            ],
            [
                "title" => 'Uji Coba Sertifikasi Skema Baru Untuk KSPPS',
                "desc" => 'Kegiatan Uji Coba Sertifikasi Skema Baru untuk KSPPS. Terima Kasih untuk pihak - pihak yang turut serta melancarkan kegiatan uji coba skema baru ini.',
                "status" => "publish",
                'slug' => Str::slug("Uji Coba Sertifikasi Skema Baru Untuk KSPPS", "-"),
                'images' => [
                    '5/1.jpeg',
                    '5/2.jpeg',
                    '5/3.jpeg',
                    '5/4.jpeg',
                ]
            ],
            [
                "title" => 'Uji Manajer di Slawi - Jawa Tengah 20 - 23 Juli 2020',
                "desc" => 'Uji Manajer di Slawi dengan jumlah peserta 32, dan tidak lupa menerapkan protokol kesehatan di era new normal ini ',
                "status" => "publish",
                'slug' => Str::slug("Uji Manajer di Slawi - Jawa Tengah 20 - 23 Juli 2020", "-"),
                'images' => [
                    '6/1.jpeg',
                    '6/2.jpeg',
                    '6/3.jpeg',
                    '6/4.jpeg',
                ]
            ],
            [
                "title" => 'Recognition Current Competency (RCC) Skema Manajer Di Bojonegoro',
                "status" => 'publish',
                "desc" => 'Lembaga Sertifikasi Profesi Koperasi Jasa Keuangan, telah melakukan kegiatan Recognition Current Competency (RCC) :\r\n\r\nSkema : Manajer \r\n\r\nTUK Sewaktu : Hotel Bojonegoro \r\n\r\nTanggal : 09 Oktober 2020 \r\n\r\n\r\n\r\nKegiatan ini sudah mengikuti protokol kesehatan (social distancing) \r\n\r\nTerima Kasih dan Jaga Kesehatan untuk semuanya jangan lupa make masker dan hand sanitizer.',
                'slug' => Str::slug("Recognition Current Competency (RCC) Skema Manajer Di Bojonegoro", "-"),
                'images' => [
                    '7/1.jpeg'
                ]
            ],
            [
                "title" => 'Uji Kompetensi Skema Manajer Di Bojonegoro - Jawa Timur',
                "status" => 'publish',
                "desc" => 'Lembaga Sertifikasi Profesi Koperasi Jasa Keuangan, telah melakukan kegiatan Uji Kompetensi :\r\n\r\nSkema : Manajer \r\n\r\nTUK Sewaktu : Hotel Bojonegoro \r\n\r\nTanggal : 09 Oktober 2020 \r\n\r\n \r\n\r\nKegiatan ini sudah mengikuti protokol kesehatan (social distancing) \r\n\r\nTerima Kasih dan Jaga Kesehatan untuk semuanya jangan lupa make masker dan hand sanitizer.',
                "title" => 'Uji Kompetensi Skema Manajer Di Bojonegoro - Jawa Timur',
                'slug' => Str::slug("Uji Kompetensi Skema Manajer Di Bojonegoro - Jawa Timur", "-"),
                'images' => [
                    '8/1.jpeg'
                ]
            ],
            [
                "title" => 'Uji Kompetensi Skema Manajer Di Daerah Istimewa Yogyakarta',
                "status" => 'publish',
                "desc" => 'Lembaga Sertifikasi Profesi Koperasi Jasa Keuangan, telah melakukan kegiatan Uji Kompetensi :\r\n\r\nSkema : Manajer \r\n\r\nTUK Sewaktu : Hotel Tara Yogyakarta \r\n\r\nTanggal : 08 - 09 Oktober 2020 \r\n\r\nJumlah Peserta : 32 orang \r\n\r\n \r\n\r\nKegiatan ini sudah mengikuti protokol kesehatan (social distancing) \r\n\r\nTerima Kasih dan Jaga Kesehatan untuk semuanya jangan lupa make masker dan hand sanitizer.',
                'slug' => Str::slug("Uji Kompetensi Skema Manajer Di Daerah Istimewa Yogyakarta", "-"),
                'images' => [
                    '9/1.jpeg'
                ]
            ]
        ];

        foreach ($events as $eventItem) {
            $event = EventActivity::create([
                'title' => $eventItem["title"],
                'desc' => $eventItem["desc"],
                'slug' => $eventItem["slug"],
                'status' => $eventItem["status"]
            ]);
            if (isset($eventItem['images'])) {
                foreach ($eventItem['images'] as $image) {
                    $event->addMedia(base_path() . '/image-bank/events/' . $image)
                        ->preservingOriginal()
                        ->toMediaCollection('events');
                }
            }
        }
    }
}
