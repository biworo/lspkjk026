<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(40)->create();
        $this->call([
            UserSeeder::class,
            SectorSeeder::class,
            SchemeSeeder::class,
            ProvinceSeeder::class,
            CertificateSeeder::class,
            EventActivitySeeder::class,
            PostSeeder::class,
            PageSeeder::class,
            AssesorSeeder::class
        ]);
    }
}
