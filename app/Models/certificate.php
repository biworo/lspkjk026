<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class certificate extends Model
{
    use HasFactory;
    public $fillable = ['no_reg', 'name', 'phone', 'gender', 'no_cert', 'no_blanko', 'scheme_id', 'email', 'province_id', 'sector_id', 'expire_year', 'year'];
    protected $dates = ['expire_date'];

    public function getGenderFullAttribute()
    {
        return $this->gender == 'm' ? 'Laki - Laki' : 'Perempuan';
    }

    public function setExpireDateAttribute($value) {
        $this->attributes['expire_date'] = Carbon::createFromFormat('d/m/Y', $value);
    }
}
