<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assesor extends Model
{
    use HasFactory;
    public $fillable = ['no_registration', 'name', 'no_certificate', 'no_blanko', 'email', 'handphone', 'sector_id', 'province_id', 'year'];
}
