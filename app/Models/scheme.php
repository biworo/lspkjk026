<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class scheme extends Model
{
    use HasFactory;
    public $fillable = ['code', 'name', 'icon'];

    public function schemeDetails() {
        return $this->hasMany(SchemeDetail::class);
    }
}
