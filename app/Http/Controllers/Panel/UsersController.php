<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{


    public function index(Request $request)
    {
        $email = $request->query('email');
        $name = $request->query('name');

        $users = DB::table('users')
            ->where('email', 'like', "%" . $email . "%")
            ->where('name', 'like', "%" . $name . "%")
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('pages/panel/users/index', compact('users', 'email', 'name'));
    }

    public function create(Request $request)
    {
        // dump($errors);
        return view('pages/panel/users/create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ]);
        $user = User::create([
            "name" => $request->full_name,
            "email" => $request->email,
            "password" => Hash::make($request->password)
        ]);
        return redirect()->route('users')->with('success', "Data pengguna berhasil disimpan.");
    }

    public function edit(User $user)
    {
        return view('pages/panel/users/edit', compact('user'));
    }

    public function update(User $user, Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'email' => 'required|unique:users,id',
        ]);
        $updated_data = [
            "name" => $request->full_name,
            "email" => $request->email
        ];
        if (!empty($user->password)) {
            $updated_data["password"] = Hash::make($request->password);
        } else {
            $request->except($updated_data, ['password']);
        }
        $user = $user->update($updated_data);
        return redirect()->back()->with("success", "Data pengguna berhasil diperbarui.");
    }

    public function delete(User $user)
    {
        if ($user->delete()) {
            return redirect()->back()->with('success', _("Berhasil menghapus data user."));
        }
        return redirect()->back();
    }
}
