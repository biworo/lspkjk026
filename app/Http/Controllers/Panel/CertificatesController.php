<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CertificatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $certificates = DB::table('certificates')
            ->join('provinces', 'certificates.province_id', '=', 'provinces.id')
            ->join('schemes', 'certificates.scheme_id', '=', 'schemes.id')
            ->join('sectors', 'certificates.sector_id', '=', 'sectors.id')
            ->select('certificates.*', 'provinces.name as province_name', 'schemes.name as scheme_name', 'schemes.code as scheme_code', 'sectors.name as sector_name')
            ->where("certificates.name", "LIKE", "%" . $request->name . "%")
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('pages/panel/certificates/index', compact('certificates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Cache::remember('province-lists', 60 * 60 * 24, function () {
            $provinces = DB::table('provinces')->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $provinces->toArray());
        });

        $schemes = Cache::remember('schemes-lists', 60 * 60 * 24, function () {
            $schemes = DB::table("schemes")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $schemes->toArray());
        });

        $sectors = Cache::remember('sectors-lists', 60 * 60 * 24, function () {
            $sectors = DB::table("sectors")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $sectors->toArray());
        });
        return view('pages.panel.certificates.create', compact("provinces", 'schemes', 'sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(certificate $certificate, Request $request)
    {
        
        $request->validate([
            "no_reg" => "required|unique:certificates",
            "name" => "required",
            "email" => "required | email",
            "gender" => "required",
            "no_cert" => "required",
            "province_id" => "required",
            "scheme_id" => "required",
            "sector_id" => "required",
            "scheme_id" => "required",
            "expire_year" => "required",
            "year" => "required"
        ]);

        $storedCert = $certificate->create([
            'no_reg' => $request->no_reg,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'no_cert' => $request->no_cert,
            'no_blanko' => $request->no_blanko,
            'province_id' => $request->province_id,
            'sector_id' => $request->sector_id,
            'scheme_id' => $request->scheme_id,
            'no_reg' => $request->no_reg,
            'year' => $request->year,
            'expire_year' => $request->expire_year,
        ]);
        return redirect()->route('certificates')->with("success", "Data sertifikat $storedCert->no_reg telah disimpan");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(certificate $certificate)
    {
        // dd($certificate);
        $provinces = Cache::remember('province-lists', 60 * 60 * 24, function () {
            $provinces = DB::table('provinces')->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $provinces->toArray());
        });

        $schemes = Cache::remember('schemes-lists', 60 * 60 * 24, function () {
            $schemes = DB::table("schemes")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $schemes->toArray());
        });

        $sectors = Cache::remember('sectors-lists', 60 * 60 * 24, function () {
            $sectors = DB::table("sectors")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $sectors->toArray());
        });

        return view('pages.panel.certificates.edit', compact("provinces", 'schemes', 'sectors', 'certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, certificate $certificate)
    {
        $request->validate([
            "no_reg" => "required|unique:certificates,no_reg," . $certificate->id,
            "name" => "required",
            "email" => "required | email",
            "gender" => "required",
            "no_cert" => "required",
            "province_id" => "required",
            "scheme_id" => "required",
            "sector_id" => "required",
            "scheme_id" => "required",
            "expire_year" => "required",
            "year" => "required"
        ]);

        $certificate->update([
            'no_reg' => $request->no_reg,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'no_cert' => $request->no_cert,
            'no_blanko' => $request->no_blanko,
            'province_id' => $request->province_id,
            'sector_id' => $request->sector_id,
            'scheme_id' => $request->scheme_id,
            'expre_date' => $request->year,
            'no_reg' => $request->no_reg,
            'year' => $request->year,
            'expire_year' => $request->expire_year,
        ]);

        return redirect()->back()->with('success', "Data Sertifikat berhasil $request->no_reg diperbarui");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(certificate $certificate)
    {
        $certificate->delete();
        return redirect()->back()->with("success", "Berhasil menghapus data sertifikat $certificate->reg_no ");
    }
}
