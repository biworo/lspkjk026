<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\EventActivity;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventActifitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = DB::table('event_activities')
        ->orderBy('created_at', 'desc')
        ->where("title", "LIKE", "%" . $request->name . "%")
        ->paginate(10);
        return view('pages/panel/events/index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.panel.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EventActivity $event)
    {
        $request->validate([
            'title' => 'required'
        ]);
        $newEvent = $event->create([
            'title' => $request->title,
            'desc' => $request->body,
            'status' => $request->status,
        ]);
        return redirect()->route('events.edit', ['event' => $newEvent->id])->with("success", "Berhasil menyimpan data kegiatan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EventActivity $event)
    {
        return view('pages.panel.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventActivity $event)
    {
        $request->validate([
            'title' => 'required'
        ]);
        $newEvent = $event->update([
            'title' => $request->title,
            'desc' => $request->body,
            'status' => $request->status,
        ]);
        return redirect()->back()->with("success", "Berhasil memperbarui data kegiatan");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventActivity $event)
    {
        if($event->media()) {
            $event->media()->delete();
        }
        $event->delete();
        return redirect()->back()->with("success", "Berhasil menghapus data kegiatan");
    }
}
