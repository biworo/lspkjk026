<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Assesor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AssesorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $assesors = DB::table('assesors')
                        ->join("provinces", "assesors.province_id", "=", "provinces.id")
                        ->join('sectors', 'assesors.sector_id', '=', 'sectors.id')
                        ->where("assesors.name", "LIKE", "%" . $request->name . "%")
                        ->select('assesors.*', 'provinces.name as province_name', 'sectors.name as sector_name')
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return view('pages/panel/assesors/index', compact('assesors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Cache::remember('province-lists', 60 * 60 * 24, function () {
            $provinces = DB::table('provinces')->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $provinces->toArray());
        });

        $sectors = Cache::remember('sectors-lists', 60 * 60 * 24, function () {
            $sectors = DB::table("sectors")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $sectors->toArray());
        });
        return view('pages.panel.assesors.create', compact('provinces', 'sectors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Assesor $assesor, Request $request)
    {
        $request->validate([
            "no_registration" => "required|unique:assesors",
            "name" => "required",
            "email" => "required | email",
            "no_blanko" => "required",
            "no_certificate" => "required",
            "province_id" => "required",
            "sector_id" => "required",
            "year" => "required"
        ]);

        $storedAssesor = $assesor->create([
            'no_registration' => $request->no_registration,
            'name' => $request->name,
            'email' => $request->email,
            'handphone' => $request->handphone,
            'gender' => $request->gender,
            'no_certificate' => $request->no_certificate,
            'no_blanko' => $request->no_blanko,
            'province_id' => $request->province_id,
            'sector_id' => $request->sector_id,
            'year' => $request->year,
        ]);
        return redirect()->route('assesors')->with("success", "Data assesor $storedAssesor->no_registration telah disimpan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Assesor $assesor): View
    {
        $provinces = Cache::remember('province-lists', 60 * 60 * 24, function () {
            $provinces = DB::table('provinces')->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $provinces->toArray());
        });

        $sectors = Cache::remember('sectors-lists', 60 * 60 * 24, function () {
            $sectors = DB::table("sectors")->select("id", "name")->get();
            return array_map(function ($x) {
                $item = [];
                return $item += ["value" => $x->id, "text" => $x->name];
            },  $sectors->toArray());
        });
        return view('pages.panel.assesors.edit', compact('provinces', 'sectors', 'assesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Assesor $assesor, Request $request)
    {
        $request->validate([
            "no_registration" => "required|unique:assesors,no_registration," . $assesor->id,
            "name" => "required",
            "email" => "required | email",
            "no_blanko" => "required",
            "no_certificate" => "required",
            "province_id" => "required",
            "sector_id" => "required",
            "year" => "required"
        ]);

        $updatedAssesor = $assesor->update([
            'no_registration' => $request->no_registration,
            'name' => $request->name,
            'email' => $request->email,
            'handphone' => $request->handphone,
            'gender' => $request->gender,
            'no_certificate' => $request->no_certificate,
            'no_blanko' => $request->no_blanko,
            'province_id' => $request->province_id,
            'sector_id' => $request->sector_id,
            'year' => $request->year,
        ]);
        return redirect()->back()->with("success", "Data assesor $assesor->no_registration telah diperbarui");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assesor $assesor)
    {
        $assesor->delete();
        return redirect()->back()->with("success", "Berhasil menghapus data assesor $assesor->no_registration ");
    }
}
