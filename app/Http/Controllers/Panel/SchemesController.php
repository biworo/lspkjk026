<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\scheme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchemesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schemes = DB::table('schemes')->select("id", "name", "icon")
                        ->where("name", "LIKE", "%" . $request->name . "%")
                        ->orderBy('created_at', 'DESC')->paginate(10);
        return view('pages.panel.schemes.index', compact('schemes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.panel.schemes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $scheme = scheme::create([
                'code' => $request->code,
                'name' => $request->name,
                'icon' => $request->icon
            ]);
            if (count($request->detail)) {
                foreach ($request->detail as $detail) {
                    $scheme->schemeDetails()->create($detail);
                }
            }
        });
        return redirect()->route('schemes')->with("success", "Data skema telah disimpan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(scheme $scheme)
    {
        return view('pages.panel.schemes.edit', compact('scheme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, scheme $scheme)
    {
        DB::transaction(function () use ($request, $scheme) {
            $scheme->update([
                'code' => $request->code,
                'name' => $request->name,
                'icon' => $request->icon
            ]);
            if (count($request->detail)) {
                $scheme->schemeDetails()->each(function ($detail, $key) {
                    $detail->delete();
                });
                foreach ($request->detail as $detail) {
                    $scheme->schemeDetails()->create($detail);
                }
            }
        });
        return redirect()->back()->with("success", "Berhasil memperbarui data skema.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(scheme $scheme)
    {
        DB::transaction(function () use ($scheme) {
            $scheme->schemeDetails()->each(function ($detail, $key) {
                $detail->delete();
            });
            $scheme->delete();
        });
        return redirect()->back()->with("success", "Berhasil menghapus data skema.");
    }
}
