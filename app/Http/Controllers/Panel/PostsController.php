<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = DB::table('posts')
        ->orderBy('created_at', 'desc')
        ->where("title", "LIKE", "%" . $request->name . "%")
        ->paginate(10);
        return view('pages/panel/posts/index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.panel.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required'
        ]);
        $postData = $post->create([
            'title' => $request->title,
            'body' => $request->body,
            'status' => $request->status
        ]);
        if($request->featured_image !== null) {
            $postData->addMedia($request->featured_image)
                ->toMediaCollection('post');
        }
        return redirect()->route('posts')->with("success", "Berhasil menambahkan artikel baru.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Post $post)
    {
        return view("pages.panel.posts.edit", compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required'
        ]);
        $post->update([
            'title' => $request->title,
            'body' => $request->body,
            'status' => $request->status
        ]);
        if($request->featured_image !== null) {
            $post->media()->delete();
            $post->addMedia($request->featured_image)
                ->toMediaCollection('post');
        }
        return redirect()->back()->with("success", "Berhasil memperbarui artikel.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if($post->media()) {
            $post->media()->delete();
        }
        $post->delete();
        return redirect()->back()->with("success", "Berhasil menghapus data artikel $post->title ");
    }
}
