<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\EventActivity;
use Illuminate\Http\Request;

class UploadImageController extends Controller
{
    
    public function upload(Request $request, EventActivity $event) {
        $eventData = $event->find($request->id);
        $eventData->addMedia($request->image)
                ->toMediaCollection('events');
        $eventLastMedia = $eventData->getMedia('events')->last();
        return response()->json(['id' => $request->id, 'image_id' => $eventLastMedia->id]);
    }

    public function delete(Request $request, EventActivity $event) {
        $eventData = $event->find($request->id);
        $eventImage = $eventData->getMedia('events')->where('id', '=', $request->image_id)->first();
        if($eventImage) {
            $eventImage->delete();
        }
        return response()->json(['message' => 'success']);
    }

}
