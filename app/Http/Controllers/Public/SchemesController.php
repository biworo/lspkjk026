<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\scheme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SchemesController extends Controller
{

    public function index(Request $request)
    {
        $slug = isset($request->slug) ? $request->slug : null;
        $schemeOptions = Cache::remember('schemes-options', 60 * 60 * 24, function () {
            $schemeOptions = DB::table("schemes")->select("id", "name")->get();
            $schemes = [];
            foreach ($schemeOptions as $key => $value) {
                $schemes[$value->id] = $value->name;
            }
            return $schemes;
        });
        if (!$slug) {
            // $schemeTables = Cache::remember('schemes-tables', 60 * 60 * 24, function () {
            //     return scheme::with('SchemeDetails')->get();
            // });
            $schemeTables = scheme::with('SchemeDetails')->get();
        } else {
            $schemeTables = scheme::with('SchemeDetails')->where('id', '=', $slug)->get();
        }
        $compact = compact('schemeTables', 'schemeOptions', 'slug');
        return view('pages.public.schemes.index', $compact);
    }
}
