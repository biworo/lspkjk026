<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssesorsController extends Controller
{
    
    public function index(Request $request) {
        $assesors = DB::table('assesors')
                        ->join("provinces", "assesors.province_id", "=", "provinces.id")
                        ->join('sectors', 'assesors.sector_id', '=', 'sectors.id')
                        ->where("assesors.name", "LIKE", "%" . $request->name . "%")
                        ->select('assesors.*', 'provinces.name as province_name', 'sectors.name as sector_name')
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return view('pages.public.assesors', compact('assesors'));
    }

}
