<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CertifactesController extends Controller
{

    public function index(Request $request)
    {
        $certificates = DB::table('certificates')
            ->join('provinces', 'certificates.province_id', '=', 'provinces.id')
            ->join('schemes', 'certificates.scheme_id', '=', 'schemes.id')
            ->join('sectors', 'certificates.sector_id', '=', 'sectors.id')
            ->where("certificates.name", "like", "%" . $request->name . "%")
            ->select('certificates.*', 'provinces.name as province_name', 'schemes.name as scheme_name', 'schemes.code as scheme_code', 'sectors.name as sector_name')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view('pages.public.certificates', compact('certificates'));
    }
}
