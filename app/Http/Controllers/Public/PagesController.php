<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\EventActivity;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index(string $slug)
    {
        $page = Page::where("slug", "=", $slug)
            ->firstOrFail();
        $sidebars = [
            [
                'text' => "Profil Lembaga",
                'route' => route("public.pages.read", "profil-lembaga")
            ],
            [
                'text' => "Layanan Kami",
                'route' => route("public.pages.read", "layanan-kami")
            ],
            [
                'text' => "Struktur Organisai",
                'route' => route("public.pages.read", "struktur-organisasi")
            ],
        ];
        return view("pages.public.pages.index", compact("page", "sidebars"));
    }
}
