<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\EventActivity;
use App\Models\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    public function index()
    {

        // $events = Cache::remember('home:events', 60 * 60 * 24, function () {
        //     return EventActivity::where('status', '=', 'publish')
        //         ->with('media')
        //         ->limit(4)
        //         ->orderBy('created_at', 'desc')
        //         ->get();
        // });

        // $posts = Cache::remember('home:posts', 60 * 60 * 24, function () {
        //     return DB::table("posts")
        //         ->where('status', '=', 'publish')
        //         ->limit(4)
        //         ->orderBy('created_at', 'desc')
        //         ->get();
        // });

        // $schemes = Cache::remember('home:scehemes',  60 * 60 * 24, function() {
        //     return DB::table("schemes")
        //         ->limit(8)
        //         ->orderBy('created_at', 'desc')
        //         ->get();
        // });

        $events = EventActivity::where('status', '=', 'publish')
                ->with('media')
                ->limit(4)
                ->orderBy('created_at', 'desc')
                ->get();

        $posts = Post::where('status', '=', 'publish')
                ->limit(4)
                ->orderBy('created_at', 'desc')
                ->get();

        $schemes = DB::table("schemes")
                ->limit(8)
                ->orderBy('created_at', 'desc')
                ->get();

        return view('pages.public.home', compact('events', 'posts', 'schemes'));

    }
}
