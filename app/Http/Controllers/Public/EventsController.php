<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\EventActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    public function index()
    {
        $events = EventActivity::select("id", "slug", "title", "desc", "created_at")
            ->with("media")
            ->where('status', '=', 'publish')
            ->orderBy('created_at', 'desc')
            ->paginate(10);
        return view("pages.public.events.index", compact("events"));
    }

    public function view(string $slug)
    {
        $event = EventActivity::where("slug", "=", $slug)
            ->with("media")
            ->firstOrFail();
        return view("pages.public.events.view", compact("event"));
    }
}
