<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index(Post $post)
    {

        // $posts = DB::table("posts")
        //     ->select("id", "title", "body", "image_path", "slug", "image", "created_at")
        //     ->orderBy("created_at", "desc")
        //     ->paginate(12);

        $posts = $post->where('status', '=', 'publish')->orderBy("created_at", "desc")->paginate(12);

        return view('pages.public.articles.index', compact("posts"));
    }

    public function read(string $slug)
    {
        $post = Post::where("slug", "=", $slug)->firstOrFail();
        return view('pages.public.articles.read', compact('post'));
    }
}
