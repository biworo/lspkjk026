<?php

    namespace App\Http;

    class Helpers {

        public static function genderToString(string $gender_char): string {
            if($gender_char == 'm') {
                return 'Pria';
            }
            elseif($gender_char == 'f') {
                return 'Wanita';
            }
            return 'unknown';
        }

    }