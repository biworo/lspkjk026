'use strict'
const React = require('react');
const ReactDOM = require('react-dom');
const axios = require('axios');

const e = React.createElement;

class FormContact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            contact: {},
            loading: false,
            showMessage: false,
            errors: {}
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeMessage = this.closeMessage.bind(this);
    }

    handleChange(e) {
        let contact = {...this.state.contact};
        let newContact = {};
        newContact[e.target.name] = e.target.value;
        this.setState({
            contact: {
                ...contact,
                ...newContact
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({loading: true});
        axios.post('/api/contact', this.state.contact).then(
            response => {
                this.setState({contact: {}, loading: false, showMessage: true});
            },
        ).catch(
            error => {
                if(error.response.status == 422) {
                    console.log(error.response);
                    this.setState({
                        ...this.state,
                        loading: false,
                        errors: error.response.data.errors
                    })
                }
                else {
                    console.error("Terjadi kesalahan : ", error);
                }
            }
        )
    }

    closeMessage() {
        this.setState({
            ...this.state,
            showMessage: false
        })
    }
 
    render() {
        return(
            <React.Fragment>
                {
                    this.state.showMessage &&
                        <div className='absolute left-0 top-0 right-0 bottom-0 bg-gray-800/50 text-white rounded-xl'>
                            <div className='absolute top-[50%] translate-y-[-50%] right-5 left-5 bg-green-600 text-white rounded-xl px-4 py-4'>
                                <button
                                    className="absolute right-[-8px] top-[-15px]"
                                    title="Tutup"
                                    onClick={this.closeMessage}
                                ><i className="far fa-times-circle text-2xl"></i></button>
                                Pesan terkirim, Terima kasih telah menghubungi kami, kami akan segera meresponse pesan anda melalui no telp atau email yang tertera dalam pesan anda.
                            </div>
                        </div>
                }
                
                <form action='' onSubmit={this.handleSubmit}>
                    <h3 className="contact-title">Hubungi Kami</h3>
                    <div className="form-group form-contact">
                        <input name="full_name" className="w-full" required placeholder='Nama Lengkap' value={ this.state.contact?.full_name || '' } onChange={this.handleChange} />
                        {
                            this.state.errors?.full_name &&
                                <small className="error">Nama lengkap tidak boleh kosong.</small>
                        }
                    </div>
                    <div className="form-group form-contact">
                        <input name="email" className="w-full" required placeholder='Email' value={ this.state.contact?.email || '' } onChange={this.handleChange} />
                        {
                            this.state.errors?.email &&
                                <small className="error">Email tidak boleh kosong.</small>
                        }
                    </div>
                    <div className="form-group form-contact">
                        <input name="phone" className="w-full" required placeholder='Telepon' value={ this.state.contact?.phone || '' } onChange={this.handleChange} />
                        {
                            this.state.errors?.phone &&
                                <small className="error">No telepon tidak boleh kosong.</small>
                        }
                    </div>
                    <div className="form-group form-contact">
                        <input name="organization_name" className="w-full" required placeholder='Nama Koperasi' value={ this.state.contact?.organization_name || '' } onChange={this.handleChange} />
                        {
                            this.state.errors?.organization_name &&
                                <small className="error">Nama koperasi tidak boleh kosong.</small>
                        }
                    </div>
                    <div className="form-group form-contact">
                        <textarea
                            className="w-full"
                            rows={2}
                            placeholder="Pesan / Pertanyaan"
                            name="body"
                            value={ this.state.contact.body || '' }
                            onChange={this.handleChange}
                        ></textarea>
                        {
                            this.state.errors?.body &&
                                <small className="error">Pertanyaan tidak boleh kosong.</small>
                        }
                    </div>
                    <button 
                        className="mt-4 px-7 py-2 bg-blue-500 text-white rounded-3xl block ml-auto disabled:bg-gray-500" 
                        onClick={this.handleSubmit}
                        disabled={this.state.loading}>
                        { this.state.loading ? 'Loading' : 'Kirim' }
                    </button>
                </form>
            </React.Fragment>
        )
    }

}

const domContainer = document.querySelector("#formContact");
ReactDOM.render(e(FormContact), domContainer);