export class Autocomplete {
    
    lists;
    name;
    label;
    selector;
    container;
    selected;

    constructor(config = {}) {
        this.lists = config.lists;
        this.name = config.name;
        this.selector = config.selector;
        this.selected = config.selected;
        this.selector.classList.add("hidden")
        this.buildAutcomplete();
    }

    clearOpenContainer() {
        let autoCompletes = document.querySelectorAll(".autocomplete-container");
        for(let au of autoCompletes) {
            au.classList.add("hidden");
        }
    }

    buildAutcomplete() {

        let container = document.createElement("div");
        let lists = this.lists;
        let listData = this.lists;
        let selector = this.selector;
        let selected = this.selected;
        let inputAu = document.createElement("input");
        let parentNode = this.selector.parentNode;
        let name = this.name;
        let listDom;
        let currentFocus = -1;
        inputAu.classList.add("w-full");
        
        this.clearOpenContainer();

        inputAu.setAttribute("name", "auto-complete-" + this.name);
        inputAu.setAttribute("type", "text")
        inputAu.setAttribute("autocomplete", "off")
        selector.parentNode.append(inputAu)
        parentNode.classList.add("relative");
        container.classList.add("autocomplete-container", "absolute", "left-0", "right-0", "bg-white", "border", "border-gray-400", "max-h-[350px]", "overflow-auto", 'mt-1', 'hidden', 'z-50');

        appendItem(this.lists);

        function  appendItem(lists) {
            container.innerHTML = ""; 
            let i = 0;
            for( let list of lists) {
                listDom = document.createElement("div");
                listDom.setAttribute('id', `${list.value}-${name}`)
                listDom.setAttribute('data-index', `${i}`)
                listDom.innerHTML = '<span>' + list.text + '<span>';
                listDom.classList.add('px-4', 'py-2', 'cursor-pointer');
                listDom.addEventListener('click', function(e) {
                    e.stopPropagation();
                    inputAu.value = list.text;
                    selector.value = list.value;
                    selected = list;
                    container.classList.add('hidden');
                });
                container.append(listDom);
                i++;
            }
        }
        
        selector.parentNode.append(container);

        inputAu.addEventListener('click', function(e) {
            e.stopPropagation()
            container.classList.remove('hidden')
        });

        inputAu.addEventListener('focus', function(e) {
            let autoCompletes = document.querySelectorAll(".autocomplete-container");
            for(let au of autoCompletes) {
                au.classList.add("hidden");
            }
            container.classList.remove('hidden')
        });

        inputAu.addEventListener('input', function(e) {
            container.classList.remove('hidden')
            lists = listData;
            currentFocus = -1;
            let val = inputAu.value;
            if(!val) {
                appendItem(lists);
            }
            lists = lists.filter( list => list.text.toLowerCase().trim().indexOf(val.toLowerCase().trim()) != -1 );
            appendItem(lists);
        });

        inputAu.addEventListener('keydown', function(e) {
            let totalList = lists.length;
            console.log(totalList)
            if(e.keyCode == 40) {
                if(currentFocus == totalList - 1) {
                    return false;
                }
                currentFocus++;
                setFocus(currentFocus);
            } else if (e.keyCode == 38) {
                if(currentFocus == -1) {
                    return false;
                }
                currentFocus--;
                setFocus(currentFocus);
            } else if(e.keyCode == 13) {
                e.preventDefault();
                if(currentFocus >= 0) {
                    setSelection(currentFocus);
                }
            }
        });

        function setSelection(currentFocus) {
            const active = lists[currentFocus];
            inputAu.value = active.text;
            selector.value = active.value;
            selected = active;
            container.classList.add('hidden');
        }

        function setFocus(currentFocus) {
            let au = container.querySelectorAll('div');
            cleanFocus();
            for(let item of au) {
                item.classList.remove('bg-blue-100')
            }
            au[currentFocus].classList.add('bg-blue-100')
        }

        const options = container.querySelectorAll('div');
        for(let item of options) {
            item.classList.remove('bg-blue-100');
            item.addEventListener('mouseover', function(e) {
                cleanFocus();
                currentFocus = item.dataset.index;
                item.classList.add('bg-blue-100');
            })
        }

        function cleanFocus() {
            let au = container.querySelectorAll('div');
            for(let item of au) {
                item.classList.remove('bg-blue-100')
            }
        } 

        document.addEventListener("click", function() {
            container.classList.add("hidden");
        });

        inputAu.addEventListener('blur', function() {
            let currentData = selected;
            if(currentData?.value || currentData?.text) {
                selector.value = currentData.value;
                inputAu.value = currentData.text;
            }
            else {
                selector.value = '';
                inputAu.value = '';
            }
            console.log(currentData)
        });

    }


}