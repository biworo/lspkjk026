export function SelectAutoComplete(config = {}) {
    
    const selector = config.selector;
    const name = selector.name;
    const data = config.data;
    let selected;
    let listData = [...data];
    const parent = selector.parentNode;
    let currentFocus = -1;
    let containerId = `auto-complete-${name}`;
    let btnDropDown;
    let open = false;
    let dataItemContainerDom;
    let inputAC;
    buildComponent();

    function buildComponent() {
        // hide selector
        selector.classList.add("hidden");
        // create new auto complete select
        let containerAC = document.createElement("div");
        containerAC.setAttribute('id', containerId);
        containerAC.classList.add("flex", "mt-2");
        inputAC = document.createElement("input");
        inputAC.setAttribute("style", "margin-top: 0;border-right: none;");
        inputAC.classList.add("grow");
        inputAC.setAttribute("type", "text");
        inputAC.setAttribute("placeholder", selector.placeholder);
        btnDropDown = document.createElement('button');
        btnDropDown.innerHTML = '<i class="fas fa-angle-down"></i>';
        btnDropDown.setAttribute("type", "button");
        btnDropDown.classList.add("border", "border-gray-300", "px-4");
        if(selector.value !== '') {
            selected = listData.find( item => item.value == selector.value);
            inputAC.value = selected.text;
            selector.value = selected.value;
        }
        containerAC.append(inputAC);
        containerAC.append(btnDropDown);
        parent.append(containerAC);
    }

    function loadData(query = '') {
        currentFocus = -1;
        dataItemContainerDom = document.createElement("div");
        dataItemContainerDom.setAttribute("id", `container-${containerId}`)
        dataItemContainerDom.classList.add("absolute", "left-0", "right-0", "top-[75px]", 'bg-white', 'border', 'border-gray-200', 'py-2', 'z-50', 'max-h-[250px]', 'overflow-auto')
        if(query != '') {
            listData = data.filter( dataItem => {
                return dataItem.text.trim().toLowerCase().indexOf(query.trim().toLowerCase()) > -1
            });
        }
        else {
            listData = data;
        }
        let i = 0;
        for(let dataItem of listData) {
            let dataItemDom = document.createElement("div");
            dataItemDom.classList.add("py-2", "px-4", "hover:cursor-pointer");
            dataItemDom.innerHTML = "<span>" + dataItem.text + "</span>";
            dataItemDom.setAttribute('data-index', i);
            dataItemContainerDom.append(dataItemDom);
            i++;
        }
        return dataItemContainerDom;
    }

    function removeDropDown() {
        let container = document.querySelector(`#container-${containerId}`);
        open = false;
        if(container) {
            container.remove();
        }
    }

    function setFocus(dataItem) {
        let itemObjectDom = document.querySelector(`#container-${containerId}`).querySelectorAll('div');
        if(dataItem) {
            for(let item of itemObjectDom) {
                item.classList.remove('bg-blue-100')
            }
            dataItem.classList.add('bg-blue-100');
            currentFocus = dataItem.dataset.index;
        }
    }

    function setSelected(currSelect) {
        selected = currSelect;
        inputAC.value = currSelect?.text;
        selector.value = currSelect?.value;
    }

    function itemEvents() {
        let itemObjectDom = document.querySelector(`#container-${containerId}`).querySelectorAll('div');
        for(let dataItem of itemObjectDom) {
            dataItem.addEventListener('mouseover', () => {
                setFocus(dataItem);
                currentFocus = dataItem.dataset.index;
            });
            dataItem.addEventListener('click', () => {
                const i = dataItem.dataset.index;
                setSelected(listData[i])
            });
        }
    }

    function querySearh(e) {
        let container = document.querySelector(`#${containerId}`);
        let query = inputAC.value;
        removeDropDown()
        container.append(loadData(query));
        itemEvents()
    }

    btnDropDown.addEventListener('click', (e) => {
        e.stopPropagation();
        currentFocus = -1;
        open = !open;
        if(open) {
            let container = document.querySelector(`#${containerId}`);
            let containerData = document.querySelector(`#container-${containerId}`);
            if(containerData) {
               containerData.remove() 
            }
            container.append(loadData());
            itemEvents()
            return;
        }
        else {
            removeDropDown();
        }
    });

    function orderFocus(e) {
        let totalList = listData.length;
        if(e.keyCode == 40) {
            if(currentFocus == totalList - 1) {
                return false;
            }
            currentFocus++;
            let itemObjectDom = document.querySelector(`#container-${containerId}`).querySelector('[data-index="' + currentFocus + '"]');
            setFocus(itemObjectDom);
        } else if (e.keyCode == 38) {
            if(currentFocus == -1) {
                return false;
            }
            currentFocus--;
            let itemObjectDom = document.querySelector(`#container-${containerId}`).querySelector('[data-index="' + currentFocus + '"]');
            setFocus(itemObjectDom);
        } else if(e.keyCode == 13) {
            e.preventDefault();
            setSelected(listData[currentFocus]);
            removeDropDown()
        }
    }

    function setToDefault() {
        if(Object.keys(selected).length) {
            setSelected(selected);
        }
        else {
            inputAC.value = '';
            selector.value = '';
        }
    }

    // inputAC.addEventListener('click', e => e.stopPropagation())
    inputAC.addEventListener('input', querySearh);
    inputAC.addEventListener('keydown', orderFocus);
    inputAC.addEventListener('blur', setToDefault);
    document.body.addEventListener("click", removeDropDown);

}