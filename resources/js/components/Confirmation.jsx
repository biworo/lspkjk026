import React, { Fragment } from 'react';

export default class Confirmation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }

    componentDidMount() {
        const deleteBtn = document.querySelectorAll('.delete-btn');

        deleteBtn.forEach( item => {
            item.addEventListener('click', () => {
                this.toggleConfirm();
            })
        });

    }

    toggleConfirm() {
        this.setState({
            ...this.state,
            show: !this.state.show
        })
    }

    render() {
        return (
            <Fragment>
            { this.state.show === true &&
                <div className='confirmation mt-9'>
                    <h1 >Hello</h1>
                </div>
            }
            </Fragment>
        )
    }

}
