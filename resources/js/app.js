require('./bootstrap');

import Alpine from 'alpinejs';
import { docReady } from "./helpers/helpers";
import 'tw-elements';

window.Alpine = Alpine;

Alpine.start();

import Swiper, { Navigation } from 'swiper';
import { Autocomplete } from './components/Autocomplete';
import autoComplete from '@tarekraafat/autocomplete.js';
import { SelectAutoComplete } from './components/SelectAutoComplete';

Swiper.use([Navigation]);

const swiper = new Swiper('.slides', {
    slidesPerView: 1,
    spaceBetween: 5,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
})

docReady( () => {
    var isHomePage = document.querySelector("body.homepage");
    var mainNav = document.querySelector("#main-nav");
    var mainLogo = document.querySelector("#main-logo");
    if(!!isHomePage) {
        document.addEventListener("scroll", () => {
            var scrollY = window.scrollY;
            if(scrollY > 200) {
                mainNav.classList.remove("absolute-nav");
                mainNav.classList.add("fixed-nav");
                mainLogo.src = "/img/logo-dark.png";
            }
            else {
                mainNav.classList.remove("fixed-nav");
                mainNav.classList.add("absolute-nav");
                mainLogo.src = "/img/logo-white.png";
            }
        });
    }

    let btnMenu = document.querySelector("#btnMenu");
    if(btnMenu) {
        let mobileMenu = document.querySelector("#mobileMenu");
        let closeMenu = document.querySelector("#closeMenu");
        let showMenu = false;
        btnMenu.addEventListener("click", toggleMenu);
        closeMenu.addEventListener("click", toggleMenu);
        function toggleMenu() {
            showMenu = !showMenu;
            if(showMenu) {
                mobileMenu.classList.add('show');
            }
            else {
                mobileMenu.classList.remove('show');
            }
        }
    }

});

if(document.querySelector("#province_id")) {
    const provinceAC = new SelectAutoComplete({
        selector: document.querySelector("#province_id"),
        data: window.provinces,
    });
}

if(document.querySelector("#scheme_id")) {
    const schemeAutoComplete = new SelectAutoComplete({
        selector: document.querySelector("#scheme_id"),
        data: window.schemes
    });
}

if(document.querySelector("#sector_id")) {
    const sectorAutoComplete = new SelectAutoComplete({
        selector: document.querySelector("#sector_id"),
        data: window.sectors
    });
}