<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Halaman') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Edit Halaman</h2>

                    @include('components.flash-message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => ['pages.update', 'page' => $page->id], 'method' => 'put', 'files' => true]) !!}
                    {!! Form::token() !!}

                    <div class="grid grid-cols-4 gap-4">
                        <div class="col-span-3">
                            <div class="form-group">
                                {!! Form::label("title", "Judul") !!}
                                {!! Form::text("title", $page->title, ["class" => 'w-full', 'required' => true]) !!}
                            </div>
                            <div class="form-group">
                                {{-- {!! Form::label("body", "Na") !!} --}}
                                {!! Form::textarea("body", $page->body, ["class" => 'w-full content-body', "required" => false]) !!}
                            </div>
                            {{-- <div class="form-group relative">
                                {!! Form::label("status", "Status") !!}
                                {!! Form::select("status", ["draft" => "Draft", "publish" => "Publish", "unpublish" => "Unpublish"], $page->status, ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih status']) !!}
                            </div> --}}
                        </div>
                    </div>


                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    {{-- <script
        src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js"
        referrerpolicy="origin"
    ></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js" integrity="sha512-WVGmm/5lH0QUFrXEtY8U9ypKFDqmJM3OIB9LlyMAoEOsq+xUs46jGkvSZXpQF7dlU24KRXDsUQhQVY+InRbncA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                tinymce.init({
                    selector: '.content-body'
                });
            });
            function selectImage() {
                var imgBtn = document.querySelector('#select-image');
                var imgFile = document.querySelector("#featured-image");
                var imgThumbContainer = document.querySelector("#image-thumbnail");
                imgBtn.addEventListener("click", function() {
                    imgFile.click();
                });
                imgFile.addEventListener('change', function(e) {
                    console.log(e.target.files[0]);
                    var src = URL.createObjectURL(e.target.files[0]);
                    var image = document.createElement("img");
                    image.setAttribute("src", src);
                    image.classList.add("w-full");
                    imgThumbContainer.innerHTML = "";
                    imgThumbContainer.append(image);
                })
            }
            selectImage();
        </script>
    @endpush
</x-app-layout>