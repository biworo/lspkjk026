<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Artikel') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Data Artikel</h2>

                    @include('components.flash-message')

                    <div class="flex justify-between my-5">
                        <div>
                            <form action="{{ route('posts') }}" class="flex">
                                <div class="mr-2">
                                    <input type="text" name="name" placeholder="Cari dengan nama" class="px-2 py-1">
                                </div>
                                <div>
                                    <button type="submit" class="btn-light">
                                        Cari
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div>
                            <a href="{{ route('posts.add') }}" class="btn-primary">
                                Tambah
                            </a>
                        </div>
                    </div>

                    <div class="overflow-auto mt-7">
                    <table class="panel-table table-fixed">
                        <thead>
                            <tr>
                                <th class="text-left">ID</th>
                                <th class="text-left">Title</th>
                                <th class="text-left">Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td class="text-left">{{ $post->id }}</td>
                                    <td class="text-left">{{ $post->title }}</td>
                                    <td class="text-left">{{ $post->status }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('posts.edit',['post' => $post->id]) }}" class="btn-icon primary mr-2"><i class="fas fa-edit"></i></a>
                                        <button class="btn-icon warn delete-btn" data-bs-toggle="modal" data-bs-target="#deleteConfirm" data-id="{{ $post->id }}"><i class="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="mt-7">
                        {{ $posts->withQueryString()->onEachSide(5)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto" id="deleteConfirm" tabindex="-1" backdrop="static" aria-labelledby="deleteConfirmTitle" aria-modal="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered relative w-auto pointer-events-none">
          <div class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
            <div class="modal-header flex flex-shrink-0 items-center justify-between p-4 border-b border-gray-200 rounded-t-md">
              <h5 class="text-xl font-medium leading-normal text-gray-800" id="exampleModalScrollableLabel">
                Konfirmasi
              </h5>
              <button type="button"
                class="btn-close box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body relative p-4">
              <p>Anda yakin ingin menghpus data artikel ini ?</p>
            </div>
            <div
              class="modal-footer flex flex-shrink-0 flex-wrap items-center justify-end p-4 border-t border-gray-200 rounded-b-md">
              <button type="button"
                class="btn-light"
                data-bs-dismiss="modal">
                Batal
              </button>
              {{ Form::open(['url' => '', 'id' => 'deletion-form']) }}
              @method("DELETE")
                <button 
                    type="submit"
                    class="btn-danger ml-2"
                >
                    Hapus
                </button>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>

      @push('scripts')
            <script type="text/javascript">
                let modalConfirm = document.querySelector("#deleteConfirm");
                modalConfirm.addEventListener("shown.bs.modal", (e) => {
                    let deletionFrm = document.querySelector('#deletion-form');
                    deletionFrm.setAttribute("action", "/panel/posts/" + e.relatedTarget.dataset.id);
                })
            </script>
      @endpush

</x-app-layout>
