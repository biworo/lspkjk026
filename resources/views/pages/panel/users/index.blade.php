<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Pengguna') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Data Pengguna</h2>

                    @include('components.flash-message')

                    <div class="flex justify-between my-5">
                        <div>
                            <form action="{{ route('users') }}" class="flex">
                                <div class="mr-2">
                                    <input type="text" name="name" value="{{ $name }}" placeholder="Cari dengan nama" class="px-2 py-1">
                                </div>
                                <button type="submit" class="bg-gray-400 px-5  py-1 text-white">
                                    Cari
                                </button>
                            </form>
                        </div>
                        <a href="{{ route('users.add') }}" class="btn-primary py-1">
                            Tambah
                        </a>
                    </div>


                    <table class="panel-table mt-8">
                        <thead>
                            <tr>
                                <th class="text-left">Nama Lengkap</th>
                                <th class="text-left">Email</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td class="text-left">{{ $user->name }}</td>
                                    <td class="text-left">{{ $user->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('users.edit',['user' => $user->id]) }}" class="btn-icon primary mr-2"><i class="fas fa-edit"></i></a>
                                        <button class="btn-icon warn delete-btn" onclick="deleteConfirm({{ $user->id }})"><i class="fas fa-trash-alt"></i></button>
                                        {{-- {!! Form::open(['route' => ['users.delete', $user->id]]) !!}
                                        @csrf
                                        @method('delete')
                                        <button class="btn-icon warn delete-btn"><i class="fas fa-trash-alt"></i></button>
                                        {!! Form::close() !!} --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-7">
                        {{ $users->withQueryString()->onEachSide(5)->links() }}
                    </div>
                    <div id="confirm"></div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    function deleteConfirm(id) {
        const delConf = confirm("Are you sure ?");
        if(delConf == 1) {
            let form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", "/users/" + id);
            var METHOD = document.createElement("input");
            METHOD.setAttribute("type", "text");
            METHOD.setAttribute("name", "_method");
            METHOD.setAttribute("value", "delete");
            form.appendChild(METHOD);
            var CSRF = document.createElement("input");
            CSRF.setAttribute("type", "text");
            CSRF.setAttribute("name", "_token");
            CSRF.setAttribute("value", "{{ csrf_token() }}");
            form.appendChild(CSRF);
            document.body.appendChild(form);
            form.submit();
        }
    }
</script>
