<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Pengguna') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Tambah Pengguna</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => 'users.store', 'method' => 'post']) !!}
                    {!! Form::token() !!}

                    <div class="form-group">
                        {!! Form::label("full_name", "Nama Lengkap") !!}
                        {!! Form::text("full_name", "", ["class" => 'w-full', 'required' => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("email", "Email") !!}
                        {!! Form::email("email", "", ["class" => 'w-full', "type" => "email", "required" => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("password", "Password") !!}
                        {!! Form::password("password", ["class" => "w-full"]) !!}
                    </div>

                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
