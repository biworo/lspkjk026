<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Kegiatan') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Edit Kegiatan</h2>

                    @include('components.flash-message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => ['events.update', 'event' => $event->id], 'method' => 'put', 'files' => true]) !!}
                    {!! Form::token() !!}

                    <div class="grid grid-cols-4 gap-4">
                        <div class="col-span-3">
                            <div class="form-group">
                                {!! Form::label("title", "Judul") !!}
                                {!! Form::text("title", $event->title, ["class" => 'w-full', 'required' => true]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label("body", "Nama Lengkap") !!}
                                {!! Form::textarea("body", $event->desc, ["class" => 'w-full content-body', "required" => false]) !!}
                            </div>
                            <div class="form-group relative">
                                {!! Form::label("status", "Status") !!}
                                {!! Form::select("status", ["draft" => "Draft", "publish" => "Publish", "unpublish" => "Unpublish"], $event->status, ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih status']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="mt-10">
                        <div class="flex items-center">
                            <h3 class="text-xl mr-2">
                                Foto Kegiatan
                            </h3>
                            <button type="button" id="btn-add"><i class="fas fa-plus"></i></button>
                            <input type="text" id="event-id" value="{{ $event->id }}" style="display: none">
                            <input type="file" name="images" accept="image/*" id="input-image" style="display: none">
                        </div>

                        <div id="list-images" class="border border-gray-200 px-5 py-5 mt-8 grid grid-cols-4 gap-4">
                            @foreach($event->getMedia('events') as $media)
                                <div class="border border-gray-100 relative bg-gray-100">
                                    <div style="background-image: url({{ $media->getUrl()}});padding-top: 50.56%;background-size: contain;background-position: center;background-repeat: no-repeat;" src="blob:http://localhost:3000/0828fbd7-5574-4b70-833f-c1ec1cd7bd28"></div>
                                    <button title="Hapus gambar" type="button" data-img-id="{{ $media->id }}" data-img-no="{{ $loop->index + 1 }}" class="absolute right-0 top-0 delete-image" onclick="javascript:deleteImage(this);"><i class="fa fa-times text-red-500"></i></button>
                                </div>
                            @endforeach
                        </div>

                    </div>


                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js" integrity="sha512-WVGmm/5lH0QUFrXEtY8U9ypKFDqmJM3OIB9LlyMAoEOsq+xUs46jGkvSZXpQF7dlU24KRXDsUQhQVY+InRbncA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script>
            window.addEventListener('DOMContentLoaded', (event) => {
                tinymce.init({
                    selector: '.content-body'
                });
            });
            function selectImage() {
                var btnAdd = document.querySelector("#btn-add");
                var inputImg = document.querySelector("#input-image");
                var listImages = document.querySelector("#list-images");
                var alertInfo = document.querySelector("#alert-info");
                var id = document.querySelector("#event-id");
                btnAdd.addEventListener('click', function() {
                    inputImg.click();
                });
                inputImg.addEventListener('change', function(e) {
                    if(this.value !== '') {
                        postNewImage(e);
                    }
                })
                function postNewImage(e) {
                    creatItemImage(e)
                }

                function uploadImage(formData) {
                    // console.log(formData);
                    return new Promise( (resolve, reject) => {
                        fetch('/api/images/upload', 
                            {
                                headers: {
                                    'Accept': 'application/json'
                                },
                                method: "POST",
                                body: formData
                            }
                        ).then( res => res.json())
                        .then(res => {
                            resolve(res);
                        }, err => {
                            reject(err);
                        })
                    });
                }

                function creatItemImage(e) {
                    var totalImage = listImages.querySelectorAll("div").length == 0 ? 1 : listImages.querySelectorAll("div").length;
                    var itemWrapper = document.createElement('div');
                    itemWrapper.classList.add('border', 'border-gray-100', 'relative', 'bg-gray-100');
                    var buttonDelete = document.createElement('button');
                    var closeIcon = document.createElement('i');
                    closeIcon.classList.add('fa', 'fa-times', 'text-red-500');
                    buttonDelete.append(closeIcon);
                    buttonDelete.setAttribute('title', 'Hapus gambar');
                    buttonDelete.setAttribute('type', 'button');
                    buttonDelete.setAttribute('data-img-no', totalImage);
                    buttonDelete.classList.add('absolute', 'right-0', 'top-0', 'delete-image');
                    buttonDelete.setAttribute('onclick', 'javascript:deleteImage(this);');
                    var src = URL.createObjectURL(e.target.files[0]);
                    var image = document.createElement('div');
                    image.setAttribute('style', 'background-image: url("' + src + '");padding-top: 50.56%;background-size: contain;background-position: center;background-repeat: no-repeat;');
                    image.setAttribute('src', src);
                    itemWrapper.append(image);
                    itemWrapper.append(buttonDelete);
                    listImages.append(itemWrapper);
                    // create form
                    var formData = new FormData();
                    formData.append('id', id.value);
                    formData.append('image', e.target.files[0]);
                    uploadImage(formData).then(res => {
                        // console.log(res);
                        buttonDelete.setAttribute('data-img-id', res.image_id);
                    }, err => {
                        console.log(err);
                    });
                }

                function onDelete(e) {
                    var id = e.taraget.data.id;
                    var image_id = e.taraget.data.image_id;
                    console.log(id, image_id);
                    console.log(e);
                }
            }
            selectImage();

            function deleteImage(e) {

                id = document.querySelector("#event-id").value;
                image_id = e.getAttribute('data-img-id');
                parent = e.parentNode;

                function deleteImageProcess(data) {
                    return new Promise( (resolve, reject) => {
                        fetch("/api/images/delete", {
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            method: 'post',
                            body: JSON.stringify(data)
                        }).then( res => res.json())
                        .then( res => {
                            resolve(res)
                        }, err => {
                            reject(err);
                        })
                    })
                }

                function removeItem() {
                    this.parent.remove()
                }

                deleteImageProcess({id: id, image_id: image_id}).then(res => {
                    if(res.message == 'success') {
                        removeItem();
                    }
                }, err => {
                    console.error(err);
                })

            }
        </script>
    @endpush
</x-app-layout>