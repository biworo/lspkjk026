<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Assesors') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Edit Assesors</h2>

                    @include('components.flash-message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => ['assesors.update', 'assesor' => $assesor->id], 'method' => 'put']) !!}
                    {!! Form::token() !!}

                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <div class="form-group">
                                {!! Form::label("no_registration", "No Registrasi") !!}
                                {!! Form::text("no_registration", $assesor->no_registration, ["class" => 'w-full', 'required' => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("name", "Nama Lengkap") !!}
                                {!! Form::text("name", $assesor->name, ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("no_certificate", "No Sertifikat") !!}
                                {!! Form::text("no_certificate", $assesor->no_certificate, ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("no_blanko", "No Blanko") !!}
                                {!! Form::text("no_blanko", $assesor->no_blanko, ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("email", "Email") !!}
                                {!! Form::text("email", $assesor->email, ["class" => 'w-full', "required" => true]) !!}
                            </div>

                        </div>
                        <div>
                            <div class="form-group">
                                {!! Form::label("handphone", "Handphone") !!}
                                {!! Form::text("handphone", $assesor->handphone, ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group relative">
                                {!! Form::label("sector_id", "Bidang") !!}
                                {!! Form::text("sector_id", $assesor->sector_id, ["class" => 'w-full cursor-pointer', 'name' => 'sector_id', 'placeholder' => 'Pilih bidang', 'id' => 'sector_id', 'autocomplete' => 'off']) !!}
                            </div>

                            <div class="form-group relative">
                                {!! Form::label("province", "Provinsi") !!}
                                {!! Form::text("province_id", $assesor->province_id, ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih provinsi', 'id' => 'province_id']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("year", "Tahun") !!}
                                {!! Form::text("year", $assesor->year, ["class" => 'w-full', "required" => true]) !!}
                            </div>
                            
                        </div>
                    </div>


                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            window.provinces = @json($provinces);
            window.sectors = @json($sectors);
        </script>
    @endpush
</x-app-layout>