<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Sertifikat') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Tambah Sertifikat</h2>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => 'certificates.store', 'method' => 'post']) !!}
                    {!! Form::token() !!}

                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <div class="form-group">
                                {!! Form::label("no_reg", "No Registrasi") !!}
                                {!! Form::text("no_reg", "", ["class" => 'w-full', 'required' => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("name", "Nama Lengkap") !!}
                                {!! Form::text("name", "", ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("email", "Email") !!}
                                {!! Form::email("email", "", ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("phone", "Handphone") !!}
                                {!! Form::text("phone", "", ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("gender", "Jenis Kelamin") !!}
                                {!! Form::select("gender", ["" => "Pilih Jenis Kelamin", "m" => "Laki - Laki", "f" => "Perempuan"], "", ["class" => 'w-full', 'required' => true]) !!}
                            </div>
                        </div>
                        <div>
                            <div class="form-group">
                                {!! Form::label("no_cert", "No sertifikat") !!}
                                {!! Form::text("no_cert", "", ["class" => 'w-full', "required" => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("no_blanko", "No Blangko") !!}
                                {!! Form::text("no_blanko", "", ["class" => 'w-full', "required" => true]) !!}
                            </div>
                            
                            <div class="form-group relative">
                                {!! Form::label("province", "Provinsi") !!}
                                {!! Form::text("province_id", "", ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih provinsi', 'id' => 'province_id']) !!}
                            </div>

                            <div class="form-group relative">
                                {!! Form::label("scheme_id", "Sekema") !!}
                                {!! Form::text("scheme_id", "", ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih sekema', 'id' => 'scheme_id']) !!}
                            </div>

                            <div class="form-group relative">
                                {!! Form::label("sector_id", "Bidang") !!}
                                {!! Form::text("sector_id", "", ["class" => 'w-full cursor-pointer', 'placeholder' => 'Pilih sekema', 'id' => 'sector_id', 'autocomplete' => 'off']) !!}
                            </div>

                            <div class="form-group relative">
                                {!! Form::label("year", "Tahun") !!}
                                {!! Form::number("year", "", ["class" => 'w-full', 'placeholder' => 'Tahun Sertifikat', 'id' => 'year']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("expire_year", "Tanggal Berakhir") !!}
                                {!! Form::number("expire_year", "", ["class" => 'w-full', 'placeholder' => 'Tahun Sertifikat', 'id' => 'year']) !!}
                            </div>
                        </div>
                    </div>


                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            window.provinces = @json($provinces);
            window.schemes = @json($schemes);
            window.sectors = @json($sectors);
        </script>
    @endpush
</x-app-layout>