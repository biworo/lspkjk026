    <x-app-layout>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Manage Sertifikat') }}
            </h2>
        </x-slot>

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200">

                        <h2 class="panel-title">Edit Sertifikat</h2>

                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert-danger mb-2" role="alert">{{ $error }}</div>
                            @endforeach
                        @endif

                        @include('components.flash-message')

                        {!! Form::model($certificate, ['route' => ['certificates.update', ['certificate' => $certificate->id]]]) !!}
                        {!! Form::token() !!}
                        @method("PUT")
                        <div class="grid grid-cols-2 gap-4">
                            <div>
                                <div class="form-group">
                                    {!! Form::label("no_reg", "No Registrasi") !!}
                                    {!! Form::text("no_reg", $certificate->no_reg, ["class" => 'w-full', 'required' => true]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("name", "Nama Lengkap") !!}
                                    {!! Form::text("name", $certificate->name, ["class" => 'w-full', "required" => true]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("email", "Email") !!}
                                    {!! Form::email("email", $certificate->email, ["class" => 'w-full', "required" => true]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("phone", "Handphone") !!}
                                    {!! Form::text("phone", $certificate->phone, ["class" => 'w-full', "required" => true]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("gender", "Jenis Kelamin") !!}
                                    {!! Form::select("gender", ["" => "Pilih Jenis Kelamin", "m" => "Laki - Laki", "f" => "Perempuan"],  $certificate->gender, ["class" => 'w-full', 'required' => true]) !!}
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    {!! Form::label("no_cert", "No sertifikat") !!}
                                    {!! Form::text("no_cert", $certificate->no_cert, ["class" => 'w-full', "required" => true]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("no_blanko", "No Blangko") !!}
                                    {!! Form::text("no_blanko", $certificate->no_blanko, ["class" => 'w-full', "required" => true]) !!}
                                </div>

                                <div class="form-group relative">
                                    {!! Form::label("province", "Provinsi") !!}
                                    {!! Form::text("province_id", $certificate->province_id, ["class" => 'w-full', 'placeholder' => 'Pilih provinsi', 'id' => 'province_id']) !!}
                                </div>

                                <div class="form-group relative">
                                    {!! Form::label("scheme_id", "Sekema") !!}
                                    {!! Form::text("scheme_id", $certificate->scheme_id, ["class" => 'w-full', 'placeholder' => 'Pilih sekema', 'id' => 'scheme_id']) !!}
                                </div>

                                <div class="form-group relative">
                                    {!! Form::label("sector_id", "Bidang") !!}
                                    {!! Form::text("sector_id", $certificate->sector_id, ["class" => 'w-full', 'placeholder' => 'Pilih sekema', 'id' => 'sector_id', 'autocomplete' => 'off']) !!}
                                </div>

                                <div class="form-group relative">
                                    {!! Form::label("year", "Tahun") !!}
                                    {!! Form::text("year", $certificate->year, ["class" => 'w-full', 'placeholder' => 'Tahun Sertifikat', 'id' => 'year']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label("expire_year", "Tanggal Berakhir") !!}
                                    {!! Form::text("expire_year",  $certificate->expire_date, ["class" => 'w-full', "required" => true]) !!}
                                </div>
                            </div>
                        </div>


                        <div class="text-right mt-4">
                            <button class="btn-primary max-w-screen-2xl">
                                Update
                            </button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                window.provinces = @json($provinces);
                window.schemes = @json($schemes);
                window.sectors = @json($sectors);
            </script>
        @endpush
    </x-app-layout>
