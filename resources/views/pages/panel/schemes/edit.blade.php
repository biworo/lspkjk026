<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Assesors') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="panel-title">Edit Skema</h2>

                    @include('components.flash-message')

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => ['schemes.update', ['scheme' => $scheme->id]], 'method' => 'put']) !!}
                    {!! Form::token() !!}

                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <div class="form-group">
                                {!! Form::label("code", "Kode Skema") !!}
                                {!! Form::text("code", $scheme->code, ["class" => 'w-full', 'required' => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("name", "Nama Skema") !!}
                                {!! Form::text("name", $scheme->name, ["class" => 'w-full', 'required' => true]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label("icon", "Icon") !!}
                                {!! Form::text("icon", $scheme->icon, ["class" => 'w-full', "required" => true]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="flex">
                        <h2 class="panel-title mt-4 mr-2">Detail Skema</h2>
                        <button type="button" id="btn-add"><i class="fas fa-plus"></i></button>
                    </div>

                    <div id="detail-lists">
                        @foreach($scheme->schemeDetails as $detail)
                        <div class="flex bg-gray-100 py-4 px-4 detail-item">
                            <div class="form-group flex-1 mr-2">
                                {!! Form::label("code", "Kode Skema") !!}
                                {!! Form::text("detail[" . $loop->index . "][code]", $detail->code, ["class" => 'w-full', 'required' => true]) !!}
                            </div>
                            <div class="form-group flex-1 mr-2">
                                {!! Form::label("title", "Judul Skema") !!}
                                {!! Form::text("detail[" . $loop->index . "][title]", $detail->title, ["class" => 'w-full', "required" => true]) !!}
                            </div>
                            {{-- <div class="form-group flex-1 mr-2">
                                {!! Form::label("training_time_duration", "Durasi Waktu Diklat (Hari)") !!}
                                {!! Form::text("detail[0][training_time_duration]", $detail->training_time_duration, ["class" => 'w-full', 'required' => true]) !!}
                            </div>
                            <div class="form-group flex-1 mr-2">
                                {!! Form::label("test_time_duration", "Durasi Waktu Uji (Hari)") !!}
                                {!! Form::text("detail[0][test_time_duration]", $detail->training_time_duration, ["class" => 'w-full', "required" => true]) !!}
                            </div> --}}
                            <div class="w-10 flex justify-center">
                                @if($loop->index >= 1)
                                    <button class="delete-item" type="button" onclick="javascript:onDelete(this);"><i class="fas fa-times text-red-500"></i></button>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>



                    <div class="text-right mt-4">
                        <button class="btn-primary max-w-screen-2xl">
                            Simpan
                        </button>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

    <script>
        function addNewSkema() {

            var btnAdd = document.querySelector('#btn-add');
            var detailLists = document.querySelector('#detail-lists');
            var detailItems = document.querySelectorAll('#detail-lists .detail-item');

            function createForm(name, text) {
                var formGroup = document.createElement('div');
                formGroup.classList.add('form-group', 'mr-2');
                var label = document.createElement('label');
                label.setAttribute('for', name)
                label.append(text);
                var input = document.createElement('input');
                input.setAttribute('name', name);
                input.setAttribute('type', 'text');
                input.setAttribute('required', true);
                input.classList.add('w-full');
                formGroup.append(label);
                formGroup.append(input);
                return formGroup;
            }

            function createDom() {
                var wrapper = document.createElement('div');
                var detailItemsCurrent = document.querySelectorAll('#detail-lists .detail-item');
                wrapper.classList.add('flex', 'bg-gray-100', 'py-4', 'px-4', 'detail-item', 'mt-2');
                var formCode = createForm('detail[' + detailItemsCurrent.length + '][code]', 'Kode Unit');
                var formTitle = createForm('detail[' + detailItemsCurrent.length + '][title]', 'Judul Skema');
                // var formTrainingTime = createForm('detail[' + detailItemsCurrent.length + '][training_time_duration]', 'Durasi Waktu Diklat (Hari)');
                // var formTrainingTest = createForm('detail[' + detailItemsCurrent.length + '][test_time_duration]', 'Durasi Waktu Uji (Hari)');
                wrapper.append(formCode);
                wrapper.append(formTitle);
                // wrapper.append(formTrainingTime);
                // wrapper.append(formTrainingTest);
                var deleteButtonWrapper = document.createElement('div');
                deleteButtonWrapper.classList.add('w-10', 'flex', 'justify-center');
                var deleteButton = document.createElement('button');
                deleteButton.classList.add('delete-item');
                deleteButton.setAttribute('type', 'button');
                deleteButton.setAttribute('onclick', 'javascript:onDelete(this);')
                var timesIcons = document.createElement('i');
                timesIcons.classList.add('fas', 'fa-times', 'text-red-500');
                deleteButton.append(timesIcons);
                deleteButtonWrapper.append(deleteButton);
                wrapper.append(deleteButtonWrapper);

                return wrapper;
            }
            function onCreateNew() {
                const newDom = createDom();
                detailLists.append(newDom);
            }

            btnAdd.addEventListener('click', onCreateNew);

        }

        addNewSkema();

        function onDelete(e) {
            var itemWrapper = e.parentNode.parentNode;
            itemWrapper.remove();
            var detailItemsCurrent = document.querySelectorAll('#detail-lists .detail-item');
            for(let [index, item] of detailItemsCurrent.entries()) {
                var inputs = item.querySelectorAll('input');
                for(let [i, input] of inputs.entries()) {
                    switch(i) {
                        case 0:
                            input.setAttribute('name', 'detail['+index+'][code]');
                        break;
                        case 1:
                            input.setAttribute('name', 'detail['+index+'][title]');
                        break;
                        // case 2:
                        //     input.setAttribute('name', 'detail['+index+'][training_time_duration]');
                        // break;
                        // case 3:
                        //     input.setAttribute('name', 'detail['+index+'][test_time_duration]');
                        // break;
                    }
                }
            }
        }

    </script>
</x-app-layout>
