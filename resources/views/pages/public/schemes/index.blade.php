<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Daftar Skema LSP-KJK") }}</h1>

    </x-slot>

    <div class="section py-12">
        <div class="bg-white shadow-md">
            <div class="flex justify-between py-7 mx-5">
                <form action="{{ route('public.assesors') }}" class="flex w-full">
                    <div class="flex w-full">
                        <div class=" mr-2 flex-grow">
                            {!! Form::select("scheme", ["" => "Semua Skema", ...$schemeOptions], $slug, ["class" => 'w-full', 'required' => true, 'id' => 'current-scheme']) !!}
                        </div>
                    </div>
                </form>
            </div>

            <div class="mt-4 mx-5 pb-5">
                @foreach ($schemeTables as $scheme)
                    <div class="scheme-item mb-7">
                        <h2 class="text-xl font-bold">Skema : {{ $scheme->name }}</h2>
                        <div class="overflow-auto">
                            <table class="panel-table table-fixed mt-3">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Unit</th>
                                        <th>Judul Unit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($scheme->SchemeDetails))
                                        @php $no = 1; @endphp
                                        @foreach ($scheme->SchemeDetails as $detail)
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $detail->code }}</td>
                                                <td>{{ $detail->title }}</td>
                                            </tr>
                                            @php $no++ @endphp
                                        @endforeach
                                    @else
                                        <tr><td colspan="6" class="text-center">Detail skema {{ $scheme->name }} tidak ditemukan.</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            // alert("Hello")
            function changeScheme() {
                var currentScheme = document.querySelector('#current-scheme');
                currentScheme.addEventListener('change', function(e) {
                    window.location.href = "/schemes/" + this.value;
                })
            }

            changeScheme();
        </script>
    @endpush

</x-pub-app-layout>
