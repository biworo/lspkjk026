<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Daftar Assesors LSP-KJK") }}</h1>

    </x-slot>

    <div class="section py-12">
        <div class="bg-white shadow-md">

            <div class="flex justify-between py-7 mx-5">
                <form action="{{ route('public.assesors') }}" class="flex w-full">
                    <div class="mr-2 flex-grow">
                        <input type="text" name="name" value="{{ request()->name ?? '' }}" placeholder="Cari dengan nama" class="px-5 py-2 w-full border-gray-400 rounded-2xl">
                    </div>
                    <button type="submit" class="bg-gray-400 px-5  py-1 text-white rounded-2xl">
                        Cari
                    </button>
                </form>
            </div>

            <div class="overflow-auto">
                <table class="panel-table table-fixed">
                    <thead>
                        <tr>
                            <th class="text-left">No Registrasi</th>
                            <th class="text-left">Nama</th>
                            <th class="text-left">No Sertifikat</th>
                            <th class="text-left">No Blanko</th>
                            <th class="text-left">Email</th>
                            <th class="text-left">Handphone</th>
                            <th class="text-left">Bidang</th>
                            <th class="text-left">Provinsi</th>
                            <th class="text-left">Tahun</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($assesors as $assesor)
                            <tr>
                                <td class="text-left">{{ $assesor->no_registration }}</td>
                                <td class="text-left">{{ $assesor->name }}</td>
                                <td class="text-left">{{ $assesor->no_certificate }}</td>
                                <td class="text-left">{{ $assesor->no_blanko }}</td>
                                <td class="text-left">{{ $assesor->email }}</td>
                                <td class="text-left">{{ $assesor->handphone }}</td>
                                <td class="text-left">{{ $assesor->sector_name }}</td>
                                <td class="text-left">{{ $assesor->province_name }}</td>
                                <td class="text-left">{{ $assesor->year }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="py-7 px-5">
                {{ $assesors->withQueryString()->onEachSide(5)->links() }}
            </div>
        </div>
    </div>

</x-pub-app-layout>