<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Artikel LSPK-JK") }}</h1>

    </x-slot>

    <div class="section py-12">
        <div class="grid grid-cols-3 gap-5">
            @foreach($posts as $post)
                <div class="block mb-4">
                    <a href="{{ route('public.articles.read', $post->slug) }}" class="block">
                        <div style="background-image: url('{{ $post->getFirstMediaUrl('post') }}')" class="pt-[56.25%] bg-gray-200 rounded-tr-3xl rounded-bl-3xl bg-center bg-cover bg-no-repeat"></div>
                        <div class="py-4">
                            <h4 class="line-clamp-2 text-lg font-bold text-gray-800">
                                {{ $post->title}}
                            </h4>
                            <small class="text-gray-700">Publish : {{ \Carbon\Carbon::parse($post->created_at)->format('d M Y H:m') }}</small>
                            <div class="mt-2 text-gray-700">
                                <p>
                                    {!! Illuminate\Support\Str::limit(strip_tags($post->body), 150, $end="...")  !!}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="mt-7">
            {{ $posts->withQueryString()->onEachSide(5)->links() }}
        </div>
    </div>
</x-pub-app-layout>
