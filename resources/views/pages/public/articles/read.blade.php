<x-pub-app-layout>
    <x-slot name="header">

        {{-- <h1 class="font-bold text-2xl text-gray-800">{{ __("Artikel LSPK-JK") }}</h1> --}}

    </x-slot>

    <div class="max-w-4xl mx-auto py-12">
        <div class="block mb-3 pl-24 relative">
            <div class="absolute left-0 top-1 pt-1 w-20 px-3">
                <div class="border-t-4 border-red-600 mb-3"></div>
                <h2 class="text-gray-900 text-2xl text-center w-full font-extrabold mb-2 block text-sm">{{ \Carbon\Carbon::parse($post->created_at)->format('d') }}</h2>
                <h2 class="text-gray-700 mb-2 text-lg text-center block text-sm">{{ \Carbon\Carbon::parse($post->created_at)->format('M') }}</h2>
                <h2 class="text-gray-700 mb-2 text-sm text-center block text-sm">{{ \Carbon\Carbon::parse($post->created_at)->format('Y') }}</h2>
                {{-- <div class="border-t-4 border-red-600 mt-4"></div> --}}
            </div>
            <h4 class="text-4xl mb-7 font-bold text-gray-800">
                {{ $post->title}}
            </h4>
            <div style="background-image: url('{{ $post->getFirstMediaUrl('post') }}')" class="pt-[56.25%] bg-gray-200 rounded-tr-3xl rounded-bl-3xl bg-center bg-cover bg-no-repeat"></div>
            <div class="py-4">
                <div class="mt-2 text-gray-700">
                    {!! $post->body !!}
                </div>
            </div>
        </div>
        </div>
    </div>
</x-pub-app-layout>
