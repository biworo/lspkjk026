<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Kegiatan LSP-KJK") }}</h1>

    </x-slot>

    <div class="">
        <div class="max-w-4xl mx-auto py-12">
            <h2 class="text-2xl mb-3 font-extrabold text-gray-700">{{ $event->title }}</h2>
            <p class="mb-5 text-sm text-gray-500 font-bold">Dipublish : {{ $event->created_at->format("d M Y") }}</p>
            <div class="block mb-3 relative">
                <div class="slides swipper">
                    <div class="swiper-wrapper">
                        @foreach ($event->media as $item)
                        <div class="swiper-slide rounded-lg">
                            <img class="rounded-lg" src="{{ $item->getUrl() }}" alt="">
                        </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
                <div class="py-4">
                    <div class="mt-2 text-gray-700">
                        {!! $event->desc !!}
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</x-pub-app-layout>
