<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Kegiatan LSP-KJK") }}</h1>

    </x-slot>

    <div class="section py-12">
        <div class="grid grid-cols-3 gap-5">
            @foreach($events as $event)
                <div class="block mb-4">
                    <a href="{{ route("public.events.view", $event->slug) }}" class="block">
                        @if(count($event->getMedia('events')))
                        @foreach($event->getMedia('events') as $media)
                            @if($loop->index === 0)
                            <div style="background-image: url('{{ $media->getUrl() }}');" class="pt-[56.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div>
                            @endif
                        @endforeach
                        @else
                        <div style="background-image: url('https://ouikar.com/pub/media/catalog/product/placeholder/default/image_not_available.png');" class="pt-[76.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div>
                        @endif
                        <div class="py-4">
                            <h4 class="line-clamp-2 text-lg font-bold text-gray-800">
                                {{ $event->title}}
                            </h4>
                            <small class="text-gray-700">Publish : {{ \Carbon\Carbon::parse($event->created_at)->format('d M Y H:m') }}</small>
                            <div class="mt-2 text-gray-700">
                                <p>
                                    {!! Illuminate\Support\Str::limit(strip_tags($event->body), 150, $end="...")  !!}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="mt-7">
            {{ $events->withQueryString()->onEachSide(5)->links() }}
        </div>
    </div>
</x-pub-app-layout>
