<x-pub-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center">
            <h1 class="font-bold text-2xl text-gray-800">{{ $page->title }}</h1>
            <small class="text-gray-700">Terakhir diperbarui : {{ \Carbon\Carbon::parse($page->updated_at)->format('d M Y') }}</small>
        </div>

    </x-slot>

    <div class="section py-12">
        <div class="flex flex-row gap-7">
            <div class="flex-none w-56 border-t-2 border-t-red-500">
                <ul class="bg-white">
                    @foreach($sidebars as $menu)
                    <li class="border-b">
                        <a class="block px-4 py-3" href="{{ $menu["route"] }}">{{ $menu["text"] }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="shrink w-full">
                <div class="bg-white shadow-md px-7 py-5">
                    <article class="text-gray-700">
                        {!! $page->body !!}
                    </article>
                </div>
            </div>
        </div>
    </div>
</x-pub-app-layout>
