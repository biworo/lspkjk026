<x-pub-app-layout>

    <div style="background-image: url('/img/banner.jpg')" class="main-banner">
        <div class="absolute left-[15%] right-[10%] top-[150px] h-[480px] z-50">
            <div class="block md:flex justify-between">
                <div class="max-w-none md:max-w-[640px]">
                    <div class="block md:flex-col h-full pt-36">
                        <h1 class="text-white text-4xl font-extrabold p-0 mr-2">LSP-KJK</h1>
                        <p class="text-white mt-3 text-xl">
                            Lembaga sertifikasi profesi koperasi jasa keuangan yang independen dan
                            siap melaksanakan sertifikasi bagi pengelola koperasi.
                        </p>
                        <p class="text-white text-xl font-bold mt-4">Pertama / Berkualitas / Berlaku Nasional</p>
                        <a href="/pages/profil-lembaga" class="block text-white underline text-lg mt-4 font-bold">Pelajari selengkapnya</a>
                    </div>
                </div>
                <div>
                <div class="w-[0px] hidden md:block md:w-[450px] bg-white px-7 py-8 rounded-xl relative" id="formContact"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="diagonal-separator bg-white">
        <div class="section text-center">
            <h2 class="section-title mb-7">Lembaga Sertifkasi Profesi Koperasi Jasa Keuangan</h2>
            <div class="text-gray-700">
                LSPK-KJK Merupakan lembaga sertifikasi independen yang melaksanakan sertifikasi kompetensi serta menerbitkan sertifikat kompetensi untuk pengelola Koperasi Simpan Pinjam/Usaha Simpan Pinjam Pola Pelayanan Konvensional (KSP/USP Koperasi) maupun Pola Pelayanan Syariah (KSPPS/USPPS Koperasi) termasuk BMT/BTM, Koperasi Pondok Pesantren yang terlisensi Badan Nasional Sertifikasi Profesi (BNSP).
            </div>
        </div>
    </div>

    <div class="bg-white relative">
        <div class="section text-center">

            <h2 class="section-title mb-3">Skema</h2>
            <p class="text-xl text-gray-500 mb-7">Berikut adalah daftar skema kami</p>
            <div class="relative mb-20 before:content-[''] before:absolute before:left-[50%] before:translate-x-[-50%] before:w-[90px] before:h-[4px] before:bg-gray-300"></div>

            <div class="flex flex-wrap gap-4 my-4 relative text-center mt-12 z-10 justify-center">
                @foreach ($schemes as $scheme)
                <a href="/schemes/{{$scheme->id}}" class="block sm:w-[100%] md:w-[100%] lg:w-[23.33%] lg:m-h-[280px]">
                    <div class="w-full bg-white border-2 border-gray-200 rounded-lg">
                        <div class="py-7 px-2">
                            <i class="{{ $scheme->icon }} text-green-600 text-5xl"></i>
                            <h5 class="line-clamp-2 mt-4 text-lg font-semibold text-gray-600">{{ Str::headline($scheme->name) }}</h5>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="my-9">
                <a href="{{ route('public.schemes') }}" class="border border-blue-500 bg-blue-500 text-white px-5 py-2 rounded-3xl text-lg font-medium">Lihat Semua</a>
            </div>
        </div>
    </div>

    <div class="bg-gray-100">
        <div class="section text-center">

            <h2 class="section-title mb-9">Kegiatan</h2>

            <div class="grid grid-cols-4 gap-4 my-4">
                @foreach ($events as $event)
                <a href="{{ route('public.events.view', $event->slug) }}">
                    <div class="w-full shadow-md bg-gray-50 rounded-lg">
                        @if(count($event->getMedia('events')))
                        @foreach($event->getMedia('events') as $media)
                            @if($loop->index === 0)
                            <div style="background-image: url('{{ $media->getUrl() }}');" class="pt-[76.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div>
                            @endif
                        @endforeach
                        @else
                        <div style="background-image: url('https://ouikar.com/pub/media/catalog/product/placeholder/default/image_not_available.png');" class="pt-[76.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div>
                        @endif
                        <div class="py-3 px-4">
                            <h5 class="line-clamp-2 text text-gray-600 text-lg">{{ Str::headline($event->title) }}</h5>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="my-9">
                <a href="{{ route('public.events') }}" class="border border-blue-500 bg-blue-500 text-white px-5 py-2 rounded-3xl text-lg font-medium">Lihat Semua</a>
            </div>

        </div>
    </div>

    <div class="bg-gray-100 mb-9">
        <div class="section text-center">

            <h2 class="section-title mb-9">Berita Terbaru</h2>

            <div class="grid grid-cols-4 gap-4 my-4">
                @foreach ($posts as $post)
                <a href="{{ route('public.articles.read', $post->slug) }}">
                    <div class="w-full shadow-md bg-gray-50 rounded-lg">
                        {{-- <div style="background-image: url('/img/posts/{{ $post->image_path }}/{{ $post->image }}')" class="pt-[76.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div> --}}
                        <div style="background-image: url('{{ $post->getFirstMediaUrl('post') }}')" class="pt-[76.25%] bg-center bg-no-repeat bg-cover rounded-tl-lg rounded-tr-lg"></div>
                        <div class="py-3 px-4">
                            <h5 class="line-clamp-2 text-lg text-gray-500">{{ Str::headline($post->title) }}</h5>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="my-9">
                <a href="{{ route('public.articles') }}" class="border border-blue-500 bg-blue-500 text-white px-5 py-2 rounded-3xl text-lg font-medium">Lihat Semua</a>
            </div>

        </div>
    </div>

    @push('scripts')
        <script src="{{ asset('js/contact.js') }}"></script>
    @endpush
</x-pub-app-layout>
