<x-pub-app-layout>
    <x-slot name="header">

        <h1 class="font-bold text-2xl text-gray-800">{{ __("Daftar Sertifikat LSP-KJK") }}</h1>

    </x-slot>

    <div class="section py-12">
        <div class="bg-white shadow-md">

            <div class="flex justify-between py-7 mx-5">
                <form action="{{ route('public.certificates') }}" class="flex w-full">
                    <div class="mr-2 flex-grow">
                        <input type="text" name="name" value="{{ request()->name ?? '' }}" placeholder="Cari dengan nama" class="px-5 py-2 w-full border-gray-400 rounded-2xl">
                    </div>
                    <button type="submit" class="bg-gray-400 px-5  py-1 text-white rounded-2xl">
                        Cari
                    </button>
                </form>
            </div>

            <div class="overflow-auto">
                <table class="panel-table table-fixed">
                    <thead>
                        <tr>
                            <th class="text-left">No Registrasi</th>
                            <th class="text-left">Nama</th>
                            <th class="text-left">Jenis Kelamin</th>
                            <th class="text-left">No Sertifikat</th>
                            <th class="text-left">No Blanko</th>
                            <th class="text-left">Kode Skema</th>
                            <th class="text-left">Nama Skema</th>
                            <th class="text-left">Bidang</th>
                            <th class="text-left">Profinsi</th>
                            <th class="text-left">Tahun Sertifikat</th>
                            <th class="text-left">Tahun Berakhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($certificates as $cert)
                            <tr>
                                <td class="text-left">{{ $cert->no_reg }}</td>
                                <td class="text-left">{{ $cert->name }}</td>
                                <td class="text-left">{{ \App\Http\Helpers::genderToString($cert->gender) }}</td>
                                <td class="text-left">{{ $cert->no_cert }}</td>
                                <td class="text-left">{{ $cert->no_blanko ? $cert->no_blanko : '-' }}</td>
                                <td class="text-left">{{ $cert->scheme_code }}</td>
                                <td class="text-left">{{ $cert->scheme_name }}</td>
                                <td class="text-left">{{ $cert->sector_name }}</td>
                                <td class="text-left">{{ $cert->province_name }}</td>
                                <td class="text-left">{{ $cert->year }}</td>
                                <td class="text-left">{{ $cert->expire_year }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="py-7 px-5">
                {{ $certificates->withQueryString()->onEachSide(5)->links() }}
            </div>
        </div>
    </div>

</x-pub-app-layout>
