@props(['label', 'lists', 'name', 'value'])

<div class="form-group relative">
    {!! Form::label($name, $label) !!}
    {!! Form::text($name, "", ["class" => 'w-full cursor-pointer', "required" => true, 'placeholder' => 'Pilih provinsi', 'id' => $name]) !!}
    {{-- <div id="autocomplate-dropdown-{{ $name }}" class="absolute top-20 left-0 right-0 z-50 h-[250px] border border-gray-100 bg-white overflow-auto hidden">
        @foreach($lists as $list)
            <div class="text-medium cursor-pointer hover:bg-gray-100 px-4 py-2">{{ $list['text'] }}</div>
        @endforeach
    </div> --}}
</div>

@push('scripts')
    <script>
        window.provinces = @json($lists);
        // let options{{$name}} = @json($lists);
        // let selector = document.querySelector('#{{ $name }}');
        // let autoCompleteDropdown{{ $name }} = document.querySelector('#autocomplate-dropdown-{{ $name }}');
        
        // window.document.addEventListener("click", function() {
        //     autoCompleteDropdown{{ $name }}.classList.add("hidden");
        // });
        // selector.addEventListener('click', function(e) {
        //     e.stopPropagation();
        //     autoCompleteDropdown{{ $name }}.classList.remove("hidden");
        // })
    </script>
@endpush