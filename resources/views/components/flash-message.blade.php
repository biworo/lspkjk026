@if ($message = Session::get('success'))
    <div class="alert-success">
        {{ $message }}
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="bg-pink-400 text-pink-800 py-2 px-4 mb-4">
        {{ $message }}
    </div>
@endif

