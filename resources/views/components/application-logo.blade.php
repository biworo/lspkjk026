@php
    $logo = request()->routeIs("homepage") ? '/img/logo-white.png' : '/img/logo-dark.png'; 
@endphp

<img src="/img/logo.png" alt="" class="main-logo">
