@props(['label', 'lists', 'name', 'value'])

<div class="form-group relative" x-data="acConfig{{ $name }}()" x-init="initAuto{{ $name }}()">
    @if (isset($label))
        {!! Form::label($label) !!}
    @endif
    <div x-on:click.away="close()">
        <input
            type="text"
            {{ $attributes->merge(['class' => 'w-full']) }}
            x-on:mousedown="open()"
            x-model="filter"
            x-on:keydown
        >
        <input type="text" name="{{ $name }}" class="hidden" x-model="selected.value" id="ac-{{ $name }}" value="{{ $value || '' }}">
    </div>
    <div class="border border-gray-300 absolute top-100 max-h-64 left-0 right-0 overflow-auto mt-2 z-50" x-show="show">
        <ul>
            <template x-for="(item, index) in filteredOptions" :key="index">
                <li class="bg-white px-4 py-2 hover:bg-gray-100 hover:cursor-pointer" x-text="item.text" x-on:click="onSelect(index)"></li>
            </template>
        </ul>
    </div>
</div>

@push('scripts')
    <script>
        function acConfig{{ $name }}() {
            return {
                filter: '',
                show: false,
                selected: { value: '', text: '' },
                focusedOptionIndex: null,
                options: @json($lists),
                initAuto{{ $name}}() {
                    // alert("Hello");
                    let current_select = this.options.find( item => item.value === {{$value}} );
                    if(!!current_select) {
                        this.selected = current_select;
                    }
                    this.filter = current_select.text;
                    console.log(this.selected);
                },
                open() {
                    this.show = true;
                    this.filter = ''
                },
                closeBlur() {
                    setTimeout(() => {
                        this.show = false;
                    }, 200);
                },
                close() {
                    this.show = false;
                    this.filter = this.selectedName();
                },
                get filteredOptions() {
                    return this.options.filter( item => item.text.trim().toLowerCase().indexOf( this.filter.trim().toLowerCase() ) !== -1);
                },
                selectedName() {
                    return this.selected?.text || '';
                },
                onSelect(index) {
                    const selected = this.filteredOptions[index];
                    this.filter = selected.text;
                    this.selected = selected;
                }
            }
        }
    </script>
@endpush
