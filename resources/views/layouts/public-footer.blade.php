<footer class="text-white">
    <div class="bg-gray-800">
        <div class="section py-14">
            <div class="grid grid-cols-4 gap-4">
                <div>
                    <h4 class="font-bold mb-3">Alamat</h4>
                    <p class="text-sm">
                        Ruko Point Automotive Center Blok D 11, Jl. Alternatif Cibubur, Harjamukti, Cimanggis, Kota Depok, Jawa Barat 16454.
                    </p>
                </div>
                <div>
                    <h4 class="font-bold mb-3">Kontak</h4>
                    <p class="text-sm">
                        Telepon: (021) 84592562 / 087780846775<br/>
                        Email : info@lspkjk026.co.id / sertifikasi.lspkjk@gmail.com
                    </p>
                </div>
                <div>
                    <h4 class="font-bold mb-3">Pintasan Link</h4>
                    <ul class="text-sm">
                        <li><a href="/pages/profil-lembaga">Tentang Kami</a></li>
                        <li><a href="/events">Kegiatan</a></li>
                        <li><a href="/schemes">Skema</a></li>
                        <li><a href="/">Kontak Kami</a></li>
                    </ul>
                </div>
                <div>
                    <h4 class="font-bold mb-3">Sosial Media</h4>
                    <ul class="text-xl flex">
                        <li class="mr-2">
                            <a href="https://instagram.com/lsp.kjk" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1536 1536"><path fill="currentColor" d="M1024 768q0-106-75-181t-181-75t-181 75t-75 181t75 181t181 75t181-75t75-181zm138 0q0 164-115 279t-279 115t-279-115t-115-279t115-279t279-115t279 115t115 279zm108-410q0 38-27 65t-65 27t-65-27t-27-65t27-65t65-27t65 27t27 65zM768 138q-7 0-76.5-.5t-105.5 0t-96.5 3t-103 10T315 169q-50 20-88 58t-58 88q-11 29-18.5 71.5t-10 103t-3 96.5t0 105.5t.5 76.5t-.5 76.5t0 105.5t3 96.5t10 103T169 1221q20 50 58 88t88 58q29 11 71.5 18.5t103 10t96.5 3t105.5 0t76.5-.5t76.5.5t105.5 0t96.5-3t103-10t71.5-18.5q50-20 88-58t58-88q11-29 18.5-71.5t10-103t3-96.5t0-105.5t-.5-76.5t.5-76.5t0-105.5t-3-96.5t-10-103T1367 315q-20-50-58-88t-88-58q-29-11-71.5-18.5t-103-10t-96.5-3t-105.5 0t-76.5.5zm768 630q0 229-5 317q-10 208-124 322t-322 124q-88 5-317 5t-317-5q-208-10-322-124T5 1085q-5-88-5-317t5-317q10-208 124-322T451 5q88-5 317-5t317 5q208 10 322 124t124 322q5 88 5 317z"/></svg>
                            </a>
                        </li>
                        <li>
                            <a href="https://web.facebook.com/lsp.koperasijasakeuangan.5" target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1536 1536"><path fill="currentColor" d="M1248 0q119 0 203.5 84.5T1536 288v960q0 119-84.5 203.5T1248 1536h-188V941h199l30-232h-229V561q0-56 23.5-84t91.5-28l122-1V241q-63-9-178-9q-136 0-217.5 80T820 538v171H620v232h200v595H288q-119 0-203.5-84.5T0 1248V288Q0 169 84.5 84.5T288 0h960z"/></svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray-900 py-2">
        <p class="font-light text-center text-sm">© {{ date("Y") }} lspkjk026.co.id All rights reserved.</p>
    </div>
</footer>
