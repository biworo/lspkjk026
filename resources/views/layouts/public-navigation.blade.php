@php    
    $nav = request()->routeIs("homepage") ? 'absolute-nav' : 'fixed-nav';
@endphp

<nav class="{{ $nav }} top-0 left-0 right-0" id="main-nav">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-12 md:h-16 py-8 md:py-11">
            <div class="flex">
                <!-- Logo -->
                <div class="shrink-0 flex items-center">
                    <a href="{{ route('homepage') }}">
                        <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-pub-nav-link :href="route('homepage')" :active="request()->routeIs('homepage')">
                        {{ __('Home') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.pages.read', 'profil-lembaga')" :active="request()->routeIs('public.pages.read')">
                        {{ __('Tentang Kami') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.articles')" :active="request()->routeIs('public.articles')">
                        {{ __('Artikel') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.events')" :active="request()->routeIs('public.events')">
                        {{ __('Kegiatan') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.certificates')" :active="request()->routeIs('public.certificates')">
                        {{ __('Sertifikat') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.assesors')" :active="request()->routeIs('public.asessors')">
                        {{ __('Assesor') }}
                    </x-pub-nav-link>
                    <x-pub-nav-link :href="route('public.schemes')" :active="request()->routeIs('public.schemes')">
                        {{ __('Skema') }}
                    </x-pub-nav-link>
                    {{-- <x-pub-nav-link :href="route('homepage')" :active="request()->routeIs('dashboard')">
                        {{ __('Kontak Kami') }}
                    </x-pub-nav-link> --}}
                </div>
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
                @if(Auth::check()) 
                    <a href="/panel/dashboard" class="nav-item flex items-center">
                        <i class="far fa-user mr-2 pb-1"></i>
                        <span class="block">{{ Auth::user()->name}}</span>
                    </a>
                @else 
                    <a href="{{ route('login') }}" class="nav-item">
                        Login
                    </a>
                @endif
            </div>

            <!-- Hamburger -->
            <div class="flex items-center sm:hidden">
                <button class="menu-bars" id="btnMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="mt-3 space-y-1">
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-responsive-nav-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Log Out') }}
                    </x-responsive-nav-link>
                </form>
            </div>
        </div>
    </div>
</nav>
