<div id="mobileMenu" class="mobile-menu">
    <div class="flex justify-end px-5 py-4">
        <button id="closeMenu">
            <i class="fas fa-times text-2xl"></i>
        </button>
    </div>
    <div class="px-5 py-4">
        <ul class="w-full">
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/">Home</a>
            </li>
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/pages/profil-lembaga">Tentang Kami</a>
            </li>
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/articles">Artikel</a>
            </li>
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/events">Kegiatan</a>
            </li>
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/certificates">Sertifikat</a>
            </li>
            <li>
                <a class="block mb-4 text-xl font-bold text-center" href="/schemes">Skema</a>
            </li>
        </ul>
    </div>
</div>