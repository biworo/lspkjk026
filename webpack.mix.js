const autoprefixer = require('autoprefixer');
const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss'); /* Add this line at the top */
const nestedPostCss = require('postcss-nested');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
// .react()
// .postCss('resources/css/app.css', 'public/css', [

// ])
// // .sass('resources/scss/app.scss', 'public/css')
// .browserSync({
//     proxy: 'localhost:8000'
// })
// .options({
//     postCss: [
//         autoprefixer,
//         tailwindcss('./tailwind.config.js')
//     ],
// }).version();

mix.js('resources/js/app.js', 'public/js')
.react()
.js('resources/js/contact.js', 'public/js')
.postCss('resources/css/app.css', 'public/css', [
    autoprefixer,
    nestedPostCss,
    tailwindcss('./tailwind.config.js')
])
.browserSync({
    proxy: 'http://127.0.0.1:8000'
});